#include "../include/Archivo.h"

// Cabeceras que tendra el CSV de CIERREs
const string Archivo::CABECERA_CIERRE_CSV = "INICIO;FIN;CONTRATO;PERIODO;ACTIVA_ABS;ACTIVA_INC;INCID_A;REACT1;REACT1_I;INCID_R1;REACT2;REACT2_I;INCID_R2;MAXPOT;FECHAMP;INCID_MAXPOT;EXCESO;INCID_EXC";
// Cabeceras que tendra el CSV de CURVAs
const string Archivo::CABECERA_CURVA_CSV = "TIMESTAMP;NHORA;ACTIVA_I;INCID1;ACTIVA_E;INCID2;REACTQ1;INCID3;REACTQ2;INCID4;REACTQ3;INCID5;REACTQ4;INCID6;RESERVA1;INCID7;RESERVA2;INCID8";

// Constructor por defecto
Archivo::Archivo() { }

// Metodo que escribira una linea en el fichero pasado por parametro
void Archivo::escribir(string strNombreFichero, string strLinea) {

    // Comprobamos si el fichero no existe para añadirle las cabeceras
    if (Archivo::existeFichero(strNombreFichero) == false) {

        // Abrimos el fichero
        ofstream oFichero;
        oFichero.open(strNombreFichero, ios_base::app);
        
        // Comprobamos que el fichero esta abierto
        if (oFichero.is_open()) {

            // Fichero de CIERREs
            std::size_t found = strNombreFichero.find("_C1");
            if (found != std::string::npos) {

                // Escribimos la cabecera de CIERREs
                oFichero << CABECERA_CIERRE_CSV;

            }

            // Fichero de CURVAs
            found = strNombreFichero.find("_TM");
            if (found != std::string::npos) {

                // Escribimos la cabecera de CURVAs
                oFichero << CABECERA_CURVA_CSV;

            }

            // Escribimos
            oFichero << strLinea;

        }
    
    } else { // El fichero ya existe

        // Abrimos el fichero
        ofstream oFichero;
        oFichero.open(strNombreFichero, ios_base::app);
        
        // Comprobamos que el fichero esta abierto
        if (oFichero.is_open()) {
            
            // Escribimos
            oFichero << strLinea;

        }

    }

}

// Metodo que comprobara si el fichero ya existe
bool Archivo::existeFichero(string strNombreFichero) {

    ifstream iFichero(strNombreFichero);

    return (bool)iFichero;

}
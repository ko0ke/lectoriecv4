#include "../include/Configuracion.h"

// Comunicacion GPRS
string  Configuracion::ip;
int     Configuracion::puerto;

// Comunicacion GSM
string  Configuracion::telefono;
string  Configuracion::puertoCOM;

// Parametros comunes
int     Configuracion::dirEnlace;
int     Configuracion::puntoMedida;
int     Configuracion::clave;
string  Configuracion::nombreFicheroLog;
int     Configuracion::debug;

// Operacion
string  Configuracion::operacion;

// Tipo de comunicacion que se va a realizar
string  Configuracion::tipoComunicacion;

// Vector que almacenara las solicitudes a realizar al contador
vector<Solicitud>   Configuracion::vSolicitudes;

// Constructor por defecto
Configuracion::Configuracion() { 

}

bool Configuracion::configurar(string strParametros) {

    string strParametro = "";
    string strError = "Sin Errores, Todo Ok.";
    string strFechaInicio = "";
    string strDelimitador = ";";
    int contador = 0;
    int contadorAux = 0;
    int codError = 0;
    int numero = -1;
    bool configurado = true;
    size_t found;
    vector<string> vTipoSolicitud{"d", "s", "r", "p", "i", "c", "h"};
    vector<string> vParametros;
    // Hacemos un split de los parametros con el caracter ';' y recorremos cada uno de ellos
    stringstream ssParametros(strParametros);
    
    // Hacemos un split de los parametros con el caracter ';' los introducimos en un vector
    while ((found = strParametros.find(strDelimitador)) != string::npos) {

        vParametros.push_back(strParametros.substr(0, found));
        strParametros.erase(0, found + strDelimitador.length());

    }
    vParametros.push_back(strParametros);

    // Guardamos los parametros genericos para todas las llamadas
    //
    // IP o Telefono
    //
    // Comprobamos si el parametro es una IP
    found = vParametros[0].find(".");
    if (found != string::npos) {

        // Comunicacion GPRS
        this->ip = vParametros[0];
        this->tipoComunicacion = "GPRS";

    } else { 

        // Comunicacion GSM
        this->telefono = vParametros[0];
        this->tipoComunicacion = "GSM";

    }

    //
    // Puerto o Puerto COM
    //
    if (this->tipoComunicacion.compare("GPRS") == 0) {

        numero = Fecha::stringToInt(vParametros[1]);
        if (numero == -1) {

            configurado = false;
            // Error
            codError = 8;
            strError += "Error en el puerto.";

        } else {

            this->puerto = numero;
        
        }

    } else {

        this->puertoCOM = "\\\\.\\"+vParametros[1];

    }

    //
    // Direccion de Enlace
    //
    numero = Fecha::stringToInt(vParametros[2]);
    if (numero == -1) {

        configurado = false;
        // Error
        codError = 8;
        strError += "Error en la Direccion de Enlace.";

    } else {

        this->dirEnlace = numero;

    }

    //
    // Punto de Medida
    //

    //
    // Clave
    //

    //
    // Modo Debug
    //

    //
    // Nombre de Fichero
    //

    //
    // Solicitud
    //

    for (int i = 0; i < vParametros.size(); i++) {

        //cout << vParametros[i] << endl;

    }


    while (getline(ssParametros, strParametro, ';')) {
        
        // Como los parametros nos llegaran en orden, tenemos en cuenta su posicion
        //
        // IP o Telefono
        //
        if (contador == 0) {

            // Comprobamos si el parametro es una IP
            // found = strParametro.find(".");
            // if (found != string::npos) {

            //     // Comunicacion GPRS
            //     this->ip = strParametro;
            //     this->tipoComunicacion = "GPRS";

            // } else { 

            //     // Comunicacion GSM
            //     this->telefono = strParametro;
            //     this->tipoComunicacion = "GSM";

            // }
        //
        // Puerto o Puerto COM
        //
        } else if (contador == 1) { 

            // if (this->tipoComunicacion.compare("GPRS") == 0) {

            //     numero = Fecha::stringToInt(strParametro);
            //     if (numero == -1) {

            //         configurado = false;
            //         // Error
            //         codError = 8;
            //         strError += "Error en el puerto.";

            //     } else {

            //         this->puerto = numero;
                
            //     }

            // } else {

            //     this->puertoCOM = "\\\\.\\"+strParametro;

            // }
        //
        // Direccion de Enlace
        //
        } else if (contador == 2) { 

            // numero = Fecha::stringToInt(strParametro);
            // if (numero == -1) {

            //     configurado = false;
            //     // Error
            //     codError = 8;
            //     strError += "Error en la Direccion de Enlace.";

            // } else {

            //     this->dirEnlace = numero;

            // }
        //
        // Punto de Medida
        //
        } else if (contador == 3) { 

            numero = Fecha::stringToInt(strParametro);
            if (numero == -1) {

                configurado = false;
                // Error
                codError = 8;
                strError += "Error en el Punto de Medida.";

            } else {

                this->puntoMedida = numero;

            }
        //
        // Clave
        //
        } else if (contador == 4) { 

            numero = Fecha::stringToInt(strParametro);
            if (numero == -1) {

                configurado = false;
                // Error
                codError = 8;
                strError += "Error en la Clave.";

            } else {

                this->clave = numero;

            }



        //
        // Modo Debug
        //
        } else if (contador == 5) { 

            numero = Fecha::stringToInt(strParametro);
            if ( (numero < 0) || (numero > 2) ) {

                configurado = false;
                // Error
                codError = 8;
                strError += "Error en el parametro DEBUG.";

            } else {

                this->debug = numero;

            }


        
        //
        // Nombre de Fichero
        //
        } else if (contador == 6) { 

            if (strParametro.compare("") != 0) {

                this->nombreFicheroLog = strParametro;

            }
        //
        // Solicitud
        //
        } else if (contador == 7) { 

            //
            // Tipos de solicitud
            //
            // d -> descarga
            // s -> configuracion del contador
            // r -> reinicio del modem
            // p -> ping
            // i -> valores instantaneos
            // c -> comprobar parametros del contador
            // h -> fecha y hora del contador
            // Buscamos en el vector de IDENTIFICADORES DE TIPOS el que hemos enviado
            if(find(vTipoSolicitud.begin(), vTipoSolicitud.end(), strParametro) != vTipoSolicitud.end()) {

                this->operacion = strParametro;

            } else {
                
                configurado = false;
                // Error
                codError = 8;
                strError += "Error en el Tipo de Solicitud.";

            }


        } else { 

            if (this->operacion.compare("d") == 0) {

                // Montamos el vector que almacenara las solicitudes al contador
                //
                // Tipo de Curva
                //
                if (contadorAux == 0) { 

                    if (strParametro.compare("1") == 0) {

                        this->solicitud.setTipoCurva("TM1");

                    } else if (strParametro.compare("2") == 0) {

                        this->solicitud.setTipoCurva("TM2");

                    } else if (strParametro.compare("3") == 0) {

                        this->solicitud.setTipoCurva("C1");

                    } else {

                        configurado = false;
                        // Error
                        codError = 8;
                        strError += "Error en el Tipo de Curva.";

                    }


                //
                // Modo de Lectura
                //
                } else if (contadorAux == 1) {

                    if ( (this->solicitud.getTipoCurva().compare("C1") != 0) && (strParametro.compare("a") == 0) ) {

                        // Absoluta
                        this->solicitud.setModoLectura("absoluta");

                    } else if ( (this->solicitud.getTipoCurva().compare("C1") != 0) && (strParametro.compare("i") == 0) ) {

                        // Incremental
                        this->solicitud.setModoLectura("incremental");

                    } else if ( (this->solicitud.getTipoCurva().compare("C1") == 0) && (strParametro.compare("t") == 0) ) {

                        // Todos los contratos del Cierre
                        this->solicitud.setModoLectura("contratoT");

                    } else if ( (this->solicitud.getTipoCurva().compare("C1") == 0) && (strParametro.compare("c1") == 0) ) {

                        // Cierre Contrato 1
                        this->solicitud.setModoLectura("contrato1");

                    } else if ( (this->solicitud.getTipoCurva().compare("C1") == 0) && (strParametro.compare("c2") == 0) ) {

                        // Cierre Contrato 2
                        this->solicitud.setModoLectura("contrato2");

                    } else if ( (this->solicitud.getTipoCurva().compare("C1") == 0) && (strParametro.compare("c3") == 0) ) {

                        // Cierre Contrato 3
                        this->solicitud.setModoLectura("contrato3");

                    }


                //
                // Fecha Inicial
                //
                } else if (contadorAux == 2) {

                    // Comprobamos que la fecha tenga el formato correcto
                    if (Fecha::validaFecha(strParametro) == true) {

                        strFechaInicio = strParametro;
                        this->solicitud.setFechaInicio(strParametro);

                    } else {

                        configurado = false;
                        // Error
                        codError = 8;
                        strError += "Error en la Fecha de Inicio.";

                    }
                //
                // Fecha Final
                //
                } else if (contadorAux == 3) {

                    // Comprobamos que la fecha tenga el formato correcto
                    if (Fecha::validaFecha(strParametro) == true) {

                        if ( (strFechaInicio.compare("") != 0) && (strParametro.compare("") != 0) && (Fecha::comparaFechas(strFechaInicio, strParametro) == 1) ) {

                            this->solicitud.setFechaFin(strParametro);
                            this->solicitud.setReferencia(-1);
                            // Añadimos el primer registro a descargar
                            this->solicitud.setFechaSiguienteCurva(primerRegistro(this->solicitud.getFechaInicio(), this->solicitud.getTipoCurva()), "");

                            this->vSolicitudes.push_back(solicitud);

                            this->solicitud.setDefault();

                        } else {

                            configurado = false;
                            if (strFechaInicio.compare("") != 0) {

                                // Error
                                codError = 8;
                                strError += "La Fecha Final es menor que la Fecha Inicial.";

                            }
                            
                        }

                    } else {

                        configurado = false;
                        // Error
                        codError = 8;
                        strError += "Error en la Fecha Final.";

                    }

                    // Reseteamos el contador de valores de la solicitud
                    contadorAux = -1;


                } else {

                }

                // Aumentamos el contador de valores de la solicitud
                contadorAux = contadorAux + 1;

            }

        }

        // Aumentamos el contador de valores de parametros
        contador = contador + 1;

    }

    contador = 1;

    if (this->debug != 0) {

        string strCabecera = "";

        strCabecera += "#################################################################################################\n";
        strCabecera += "##\n";
        strCabecera += "##\tPARAMETROS DE CONEXION\n";
        strCabecera += "#################################################################################################\n";
        strCabecera += "##\tCOMUNICACION: " + this->tipoComunicacion + "\n";

        if (this->tipoComunicacion.compare("GSM") == 0) {

            strCabecera += "##\tTELEFONO: " + this->telefono + "\t\t\t\tCOM: " + this->puertoCOM + "\n";

        } else if (this->tipoComunicacion.compare("GPRS") == 0) {

            strCabecera += "##\tIP: " + this->ip + "\t\t\t\tPUERTO: " + to_string(this->puerto) + "\n";
        }

        strCabecera += "##\tDIR ENLACE: " + to_string(this->dirEnlace) + "\t\t\tP MEDIDA: " + to_string(this->puntoMedida) + "\t\t\tCLAVE: " + to_string(this->clave) + "\n";
        strCabecera += "#################################################################################################\n";
        strCabecera += "##\n";
        strCabecera += "##\tSOLICITUDES A REALIZAR\n";
        strCabecera += "#################################################################################################\n";
        
        // Imprimimos la solicitudes a realizar
        for (int i = 0; i < this->vSolicitudes.size(); i++) {

            // Cremos el nombre del fichero para cada una de las solicitudes. El formato es el siguiente:
            // ESTAMPATIEMPO+NºSOLICITUD_IPoTELEFONO_DIRENLACE_TIPOCURVA_MODOLECTURA
            string numero = "";

            if (this->tipoComunicacion.compare("GPRS") == 0) {

                // Sustituimos los puntos de la ip por guiones para el nombre del fichero
                numero = this->ip;
                for (int i = 0; i < numero.length(); i++) {

                    if (numero[i] == '.') {

                        numero[i] = '_';

                    } 

                }

            } else if (this->tipoComunicacion.compare("GSM") == 0) {

                numero = this->telefono;

            }

            // Creamos el nombre del fichero LOG si no estaba ya definido
            if (this->nombreFicheroLog.compare("") == 0) {

                this->nombreFicheroLog = Fecha::estampaFechaActual() + "" + to_string(contador) + "_" + numero + "_" + to_string(this->dirEnlace);
            }

            this->vSolicitudes[i].setNombreFicheroCSV(this->nombreFicheroLog  + "" + to_string(contador) + "_" + this->vSolicitudes[i].getTipoCurva() + "_" + this->vSolicitudes[i].getModoLectura());
            // Archivo::escribir(this->nombreFicheroLog + ".log", "\n" + this->vSolicitudes[i].getNombreFicheroCSV());

            // Añadimos los posibles errores de la configuracion
            this->vSolicitudes[i].setCodigo(codError);
            this->vSolicitudes[i].setMensaje(strError);

            if (this->vSolicitudes[i].getTipoCurva().compare("TM1") == 0) {

                if (this->vSolicitudes[i].getModoLectura().compare("absoluta") == 0) {

                    strCabecera += "##\tCURVA HORARIA ABSOLUTA DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                } else if (this->vSolicitudes[i].getModoLectura().compare("incremental") == 0) {

                    strCabecera += "##\tCURVA HORARIA INCREMENTAL DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                }

            } else if (this->vSolicitudes[i].getTipoCurva().compare("TM2") == 0) {

                if (this->vSolicitudes[i].getModoLectura().compare("absoluta") == 0) {

                    strCabecera += "##\tCURVA CUARTA-HORARIA ABSOLUTA DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                } else if (this->vSolicitudes[i].getModoLectura().compare("incremental") == 0) {

                    strCabecera += "##\tCURVA CUARTA-HORARIA INCREMENTAL DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                }

            } else if (this->vSolicitudes[i].getTipoCurva().compare("C1") == 0) {

                if (this->vSolicitudes[i].getModoLectura().compare("contratoT") == 0) {

                    strCabecera += "##\tCIERRE CONTRATO 1, CONTRATO 2 Y CONTRATO 3 DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                } else if (this->vSolicitudes[i].getModoLectura().compare("contrato1") == 0) {

                    strCabecera += "##\tCIERRE CONTRATO 1 DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                } else if (this->vSolicitudes[i].getModoLectura().compare("contrato2") == 0) {

                    strCabecera += "##\tCIERRE CONTRATO 2 DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                } else if (this->vSolicitudes[i].getModoLectura().compare("contrato3") == 0) {

                    strCabecera += "##\tCIERRE CONTRATO 3 DESDE [" + this->vSolicitudes[i].getFechaInicio() + "] HASTA [" + this->vSolicitudes[i].getFechaFin() + "]\n";

                }
            
            
            }

            contador = contador + 1;
        
        }

        strCabecera += "#################################################################################################\n";
        strCabecera += "\n";

        Archivo::escribir(this->nombreFicheroLog + ".log", strCabecera);

    }

    return configurado;

}

string Configuracion::primerRegistro(string strFecha, string strTipoCurva) {
    char fecha[20];
    char hora[20];
    time_t curr_time;
    struct tm *tmFecha;
    time(&curr_time);
	tmFecha = localtime(&curr_time);
    string strResult = "";
    int sumar = 0;

    if (strFecha.compare("") != 0) {

        // Convertirmos la fecha a struct tm
        // dd/mm/aaaa hh:mm
        tmFecha->tm_hour = stoi(strFecha.substr(11, 2)); // Hora
        int minuto = stoi(strFecha.substr(14, 2));
        tmFecha->tm_min = minuto; // Minuto
        tmFecha->tm_mday = stoi(strFecha.substr(0, 2)); // Dia 
        tmFecha->tm_mon = stoi(strFecha.substr(3, 2)) - 1; // Mes 
        tmFecha->tm_year = stoi(strFecha.substr(6, 4)) - 1900; // Ano

        mktime(tmFecha);

        if (strTipoCurva.compare("TM2") == 0) {

            if ( (minuto == 0) || (minuto == 15) || (minuto == 30) || (minuto == 45) ) {

                sumar = 0;

            } else {
            
                if ( (minuto > 0) && (minuto < 15) ) {

                    sumar = 15 - minuto;

                } else if ( (minuto > 15) && (minuto < 30) ) {

                    sumar = 30 - minuto;

                } if ( (minuto > 30) && (minuto < 45) ) {

                    sumar = 45 - minuto;

                } if ( (minuto > 45) && (minuto < 60) ) {

                    sumar = 60 - minuto;

                } 
            
            }
            
        } else if (strTipoCurva.compare("TM1") == 0) {

            if (minuto == 0) {

                sumar = 0;

            } else {

                sumar = 60 - minuto;
            
            }

        }

        tmFecha->tm_min += sumar;

        mktime(tmFecha);

        string strAuxFecha = "";

        strftime(fecha, sizeof(fecha), "%d/%m/%Y", tmFecha);
        strftime(hora, sizeof(hora), "%H:%M", tmFecha);

        string strDate(fecha);
        string strHora(hora);
        strResult = strDate + " " + strHora;

    }

    return strResult;
}
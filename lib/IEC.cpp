#include "../include/IEC.h"

// Constructor por defecto
IEC::IEC() {

    this->reintento = 0;

}

// Metodo que analizara la trama devuelta por el contador y si tiene informacion, la extraera
int IEC::analizar(int nSolicitud, vector<unsigned char> vTramaRecibida, vector<unsigned char> vTramaEnviada) {
    
    //
    //      CODIGOS
    //          1. Trama devuelta OK.
    //          2. Se ha recibido una trama, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama.
    //          3. No se ha recibido ninguna trama. Se realiza un reintento.
    //          4. Cortar comunicacion.
    //          5. Seguir solicitando datos.
    //          6. Dejar de solicitar datos.
    //          7. Siguiente solicitud
    //    
    int codRespuesta = 1;
    vector<unsigned char> vTramaAnalizar;

    //codRespuesta = 1;
    // Si la trama recibida esta vacia, devolvemos el codigo 3 para realizar un reintento
    if (vTramaRecibida.empty() == false) {

        // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nBusco BYTES CHUNGOS! ");

        // Limpiamos de la trama recibida los bytes que no pertenecen a ninguna trama
        for (int k = 0; k < vTramaRecibida.size(); k++) {

            // Si detectamos una trama FIJA la saltamos
            if ( vTramaRecibida[k] == this->fija.FLAG_TRAMA_FIJA) {

                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFija: [" + to_string(k) + "]");
                // Aplicamos 5 en vez de 6 por el +1 del FOR
                k = k + 5;

            } 
            // Si detectamos una trama VARIABLE la saltamos
            else if (vTramaRecibida[k] == this->variable.FLAG_TRAMA_VARIABLE) {

                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nVariable: [" + to_string(k) + "]");
                // Aplicamos 5 en vez de 6 por el +1 del FOR
                k = k + ((int)vTramaRecibida[k + 1] + 5);

            } 
            // Byte indefinido, lo eliminamos de la trama recibida
            else {

                
                // stringstream sstream;
                // sstream  << "\n\nBORRAMOS BYTE CHUNGO! ";

                // sstream << "[" << k << "]";
                // sstream << hex << setw(2) << setfill('0') << (unsigned int)vTramaRecibida[k];
                // sstream << dec << "(" << (unsigned int)vTramaRecibida[k] << ") ";


                // vTramaRecibida.erase(vTramaRecibida.begin() + k, (vTramaRecibida.begin() + k));

                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", sstream.str());
                    
            }

        }

        // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nDejo de buscar BYTES CHUNGOS! ");

        // Recorremos la trama recibida hasta que este vacia
        while (vTramaRecibida.empty() == false) {

            // Obtenemos el primer byte de la trama recibida
            unsigned char flagTramaRecibida = vTramaRecibida[0];
            int longTramaAnalizar = 0;

            // Comprobamos si hemos recibido una trama FIJA
            if (flagTramaRecibida == this->fija.FLAG_TRAMA_FIJA) {

                // Guardamos el tamaño de la trama FIJA
                longTramaAnalizar = 6;
                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSe detecta trama FIJA recibida...");

            } else if ( (flagTramaRecibida == this->variable.FLAG_TRAMA_VARIABLE) && (vTramaRecibida.size() > 2) ) { // Comprobamos si hemos recibido una trama VARIABLE

                // Guardamos el tamaño de la trama VARIABLE
                longTramaAnalizar = (vTramaRecibida[1] + 6); // 6 -> 4 de cabecera + 2 de fin
                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSe detecta trama VARIABLE recibida...");
            
            } 
            // else {

            //     // Se ha recibido una trama, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama.
            //     codRespuesta = 2;
            //     vTramaRecibida.clear();
            //     // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSe detecta trama TRAMBOLICA recibida...");

            // }

            

            // Comprobamos que podemos eliminar la trama a analizar de la trama recibida
            if ( (longTramaAnalizar != 0) && (vTramaRecibida.size() >= longTramaAnalizar) ) {

                // Hacemos una copia de la trama
                copy(vTramaRecibida.begin(), vTramaRecibida.begin() + longTramaAnalizar, back_inserter(vTramaAnalizar));

                // Eliminamos la trama a analizar de la trama recibida
                vTramaRecibida.erase(vTramaRecibida.begin(), (vTramaRecibida.begin() + longTramaAnalizar));
                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nEliminamos la trama a analizar de la trama recibida...");

                // Imprimimos la trama que vamos a analizar
                stringstream sstream;
                sstream  << "\n[RECIBIDO ANALIZAR] [" + to_string(vTramaAnalizar.size()) + "] <- ";
                for (int i = 0; i < vTramaAnalizar.size(); i++) {

                    sstream << "[" << i << "]";
                    sstream << hex << setw(2) << setfill('0') << (unsigned int)vTramaAnalizar[i];
                    sstream << dec << "(" << (unsigned int)vTramaAnalizar[i] << ") ";

                }

                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", sstream.str());

                bool duplicado = true;
                // Recorremos la trama recibida hasta que no encontremos tramas duplicadas
                while (duplicado == true) {

                    auto it = search(vTramaRecibida.begin(), vTramaRecibida.end(), vTramaAnalizar.begin(), vTramaAnalizar.end());
                    if (it != vTramaRecibida.end()) {

                        // Trama duplicada encontrada
                        duplicado = true;
                        int comienzo = it - vTramaRecibida.begin();
                        vTramaRecibida.erase(vTramaRecibida.begin() + comienzo, ((vTramaRecibida.begin() + comienzo) + longTramaAnalizar));
                        // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTrama duplicada eliminada...");

                    } else {

                        // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLa trama no esta duplicada...");
                        // No hay tramas duplicadas
                        duplicado = false;

                    }

                }

                // Comprobamos que la trama a analizar tiene la estructura correcta
                if ( ( (vTramaAnalizar[0] == this->fija.FLAG_TRAMA_FIJA) || (vTramaAnalizar[0] == this->variable.FLAG_TRAMA_VARIABLE) ) && (vTramaAnalizar[longTramaAnalizar - 1] == 0x16) ) {

                    // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLa trama tiene el formato correcto...");

                    // Obtenemos el primer byte de la trama recibida
                    unsigned char flagTramaEnviada = vTramaEnviada[0];
                    unsigned char flagTramaAnalizar = vTramaAnalizar[0];

                    // Si hemos enviado una trama FIJA
                    if (flagTramaEnviada == this->fija.FLAG_TRAMA_FIJA) {

                        // Obtenemos el campo de control de la trama FIJA enviada
                        unsigned char ccEnviado = vTramaEnviada[1];

                        // Comprobamos que tipo de trama FIJA hemos enviado
                        switch (ccEnviado)
                        {
                            case 0x49: // SOLICITUD ESTADO ENLACE

                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitud Estado Enlace...");

                                // Recibimos una trama FIJA
                                if (flagTramaRecibida == this->fija.FLAG_TRAMA_FIJA) {

                                    // Comprobamos el Campo de Control de la trama FIJA devuelta
                                    if (vTramaAnalizar[1] == 0x0B) {
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "OK");

                                        codRespuesta = 1;
                                        vTramaAnalizar.clear();
                                        vTramaRecibida.clear();

                                    } else {

                                        codRespuesta = 2;
                                        vTramaAnalizar.clear();

                                    }

                                } else { // Recibimos una trama VARIABLE

                                    codRespuesta = 2;
                                    vTramaAnalizar.clear();

                                }

                                break;

                            case 0x40: // REPOSICION ENLACE REMOTO
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReposicion Enlace Remoto...");

                                // Recibimos una trama FIJA
                                if (flagTramaRecibida == this->fija.FLAG_TRAMA_FIJA) {

                                    if (vTramaAnalizar[1] == 0x00) {
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "OK");
                                        codRespuesta = 1;

                                        vTramaAnalizar.clear();
                                        vTramaRecibida.clear();

                                    } else { 

                                        codRespuesta = 2;
                                        vTramaAnalizar.clear();

                                    }

                                } else { // Recibimos una trama VARIABLE

                                    codRespuesta = 2;
                                    vTramaAnalizar.clear();

                                }

                                break;

                            case 0x5B:
                            case 0x7B: // SOLICITD DATOS

                                // Recibimos una trama VARIABLE
                                if (flagTramaAnalizar == this->variable.FLAG_TRAMA_VARIABLE) {

                                    // Obtenemos el CAMPO DE CONTROL de la trama VARIABLE a analizar
                                    unsigned int ccTramaAnalizar = vTramaAnalizar[4];
                                    // Obtenemos el IDENTIFICADOR DE TIPO de la trama VARIABLE a analizar
                                    unsigned int idTipoTramaAnalizar = vTramaAnalizar[7];
                                    // Obtenemos la CAUSA de la trama VARIABLE a analizar
                                    unsigned int causaTramaAnalizar = vTramaAnalizar[9];
                                    // Vector que almacenara los registros recibidos
                                    vector<unsigned char> vRegistros;
                                    // Vector que almacenara la fecha recogida de la trama en hexadecimal
                                    vector<unsigned char> vFecha;
                                    // Vector que almacenara la fecha y la bandera 
                                    vector<string> vFechaCompleta;
                                    // Tamaño del registro
                                    int tamanoRegistro = 0;
                                    bool encontrado = false;
                                    int contrato = 0;
                                    stringstream strStream;

                                    // Comprobamos el IDENTIFICADOR DE TIPO de la trama VARIABLE a analizar
                                    switch (idTipoTramaAnalizar)
                                    {
                                        case 0x48: // REGISTRO DE LA FECHA DEL CONTADOR

                                            //codRespuesta = 7;

                                            copy((vTramaAnalizar.begin() + 15), (vTramaAnalizar.end() - 2), back_inserter(vRegistros));

                                            // Obtenemos la fecha del registro
                                            for (int i = 0; i < vTramaAnalizar.size(); i++) {

                                                vFecha.push_back(vRegistros[i]);

                                            }

                                            // Comprobamos que la fecha es correcta
                                            vFechaCompleta = Fecha::fechaHexToFechaStr(vFecha);

                                            
                                            strStream  << "\n[FECHA] [" + vFechaCompleta[0] + "] [" + vFechaCompleta[1] + "]";
                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", strStream.str());

                                            break;

                                        case 0xB7: // RESPUESTA AL INICIO SESION Y ENVIO CLAVE

                                            // Confirmacion de activacion ACEPTADA
                                            if ( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x07) ) {
                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConfirmacion de activacion ACEPTADA...");

                                                codRespuesta = 1;
                                                vTramaAnalizar.clear();
                                                vTramaRecibida.clear();
                                                

                                            } else if ( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x47) ) { // Confirmacion de activacion RECHAZADA
                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nERROR en el Punto Medida o Clave...");

                                                // Error en el Punto de Medida o la Clave
                                                codRespuesta = 4;
                                                vTramaAnalizar.clear();
                                                vTramaRecibida.clear();

                                            } else if( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x10) ) { // Finalizacion de la activacion
                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFinalizacion de la activacion...");

                                                // Finalizacion de la activacion
                                                codRespuesta = 4;
                                                vTramaAnalizar.clear();
                                                vTramaRecibida.clear();

                                            }

                                            break;

                                        case 0xBE: // RESPUESTA A LA SOLICITUD DE TOTALES INTEGRADOS INCREMENTALES
                                        case 0xBD: // RESPUESTA A LA SOLICITUD DE TOTALES INTEGRADOS ABSOLUTOS
                                        case 0x86: // RESPUESTA A LA SOLICITUD DE CIERRES
                                
                                            // Comprobamos que hemos enviado una trama FIJA de SOLICITUD DE DATOS
                                            if ( (vTramaEnviada[1] == 0x5B) || (vTramaEnviada[1] == 0x7B) ) {

                                                if (ccTramaAnalizar == 0x08) {

                                                    if (causaTramaAnalizar == 0x07) { // Datos de usuario RESPOND y CONFIRMACION

                                                        // Trama devuelta OK
                                                        codRespuesta = 5;

                                                        // Cambiamos el FCB
                                                        if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                            this->fija.FRAME_COUNT_BIT.set(0,1);

                                                        } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                            this->fija.FRAME_COUNT_BIT.set(0,0);

                                                        }

                                                        vTramaAnalizar.clear();
                                                    
                                                    } else {

                                                        // Dejar de solicitar datos
                                                        codRespuesta = 6;

                                                        // En la ultima solicitud, tras finalizar la sesion, no tenemos en cuenta los errores
                                                        if (nSolicitud != -1) {

                                                            // El contador ya ha devuelto todos los registros, no tiene mas
                                                            if (causaTramaAnalizar == 0x0A) {
                                                                
                                                                if (Configuracion::debug == 2) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nEl contador no tiene mas registros entre las fechas solicitadas");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x47) { // Datos solicitados no disponibles
                                                            
                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(14);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("Datos solicitados no disponibles. ");

                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nDatos solicitados no disponibles. ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x12) { // Periodo de integracion no disponible

                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(7);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("Periodo de integracion no disponible. ");

                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nPeriodo de integracion no disponible. ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x0D) { // Registro de datos solicitado no disponible
                                                                
                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(6);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("Registro de datos solicitado no disponible. ");
                
                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistro de datos solicitado no disponible. ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x0E) { // Tipo de ASDU solicitado no disponible

                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(9);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("No dispone de registros " + Configuracion::vSolicitudes[nReferencia].getModoLectura() + ". ");
                                                                
                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de registros " + Configuracion::vSolicitudes[nReferencia].getModoLectura() + ". ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x0F) { // Nº de registro en el ASDU enviado por CM desconocido

                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(10);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("No dispone de " + Configuracion::vSolicitudes[nReferencia].getTipoCurva() + " de " + Configuracion::vSolicitudes[nReferencia].getModoLectura() + ". ");

                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de " + Configuracion::vSolicitudes[nReferencia].getTipoCurva() + " de " + Configuracion::vSolicitudes[nReferencia].getModoLectura() + ". ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x10) { // Especificacion de direccion en el ASDU enviado por CM desconocida

                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(11);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("No dispone de Totales Integrados CON reservas de " + Configuracion::vSolicitudes[nReferencia].getTipoCurva() + " de " + Configuracion::vSolicitudes[nReferencia].getModoLectura() + ". ");

                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de Totales Integrados CON reservas de " + Configuracion::vSolicitudes[nSolicitud].getTipoCurva() + " de " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                }

                                                            } else if (causaTramaAnalizar == 0x11) { // Datos solicitados no disponibles

                                                                int nReferencia = nSolicitud;
                                                                if (Configuracion::vSolicitudes[nSolicitud].getReferencia() != -1) {

                                                                    nReferencia = Configuracion::vSolicitudes[nSolicitud].getReferencia();

                                                                }

                                                                Configuracion::vSolicitudes[nReferencia].setCodigo(12);
                                                                Configuracion::vSolicitudes[nReferencia].setMensaje("Objeto de informacion no disponible.");
                                                                
                                                                if (Configuracion::debug != 0) {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nObjeto de informacion no disponible. ");
                                                                
                                                                }

                                                            } else {

                                                                // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                                                codRespuesta = 2;
                                                                vTramaAnalizar.clear();

                                                            }

                                                        }

                                                    }

                                                }

                                            }
                                            
                                            break;

                                        case 0x8C: // REGISTROS DE TOTALES INTEGRADOS INCREMENTALES
                                        case 0x8B: // REGISTROS DE TOTALES INTEGRADOS ABSOLUTOS

                                            // No nos importa el tipo de trama enviada, la trama recibida
                                            // es de registros integrados (curvas), obtenemos la informacion

                                            // Eliminamos los 13 primeros bytes y los 2 ultimos
                                            copy((vTramaAnalizar.begin() + 13), (vTramaAnalizar.end() - 2), back_inserter(vRegistros));
                                            
                                            // Una vez extraida la informacion de la trama a analizar, la eliminamos
                                            vTramaAnalizar.clear();
                                            //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nvRegistros..." + to_string(vRegistros.size()));
                                            
                                            while (vRegistros.empty() == false) {

                                                vFecha.clear();
                                                vFechaCompleta.clear();

                                                // El primer BYTE nos dice que tipo de DIRECCION DE OBJETO es
                                                switch (vRegistros[0])
                                                {
                                                    case 0x09: // Totales Integrados Generales Con Reservas
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Generales Con Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                        tamanoRegistro = 46;

                                                        break;
                                                    
                                                    case 0x0A: // Totales Integrados Generales Sin Reservas
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Generales Sin Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                        tamanoRegistro = 36;

                                                        break;
                                                    
                                                    case 0x0B: // Totales Integrados Consumo Puro Sin Reservas
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Consumo Puro Sin Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                        tamanoRegistro = 21;

                                                        break;
                                                
                                                    default:
                                                        
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n####################### No se reconoce la DIRECCION DE OBJETO del registro [" + to_string((int)vRegistros[0]) + "]");
                                                        tamanoRegistro = 0;
                                                
                                                        break;
                                                }

                                                if (tamanoRegistro != 0) {

                                                    // Obtenemos la fecha del registro
                                                    for (int i = (tamanoRegistro - 5); i < tamanoRegistro; i++) {

                                                        vFecha.push_back(vRegistros[i]);

                                                    }

                                                    stringstream ss;
                                                    ss  << "\n[REGISTRO] [" + to_string(tamanoRegistro) + "] <- ";
                                                    for (int i = 0; i < tamanoRegistro; i++) {

                                                        ss << "[" << i << "]";
                                                        ss << hex << setw(2) << setfill('0') << (unsigned int)vRegistros[i];
                                                        ss << dec << "(" << (unsigned int)vRegistros[i] << ") ";

                                                    }
                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", ss.str());
                                                    
                                                    // Comprobamos que la fecha es correcta
                                                    vFechaCompleta = Fecha::fechaHexToFechaStr(vFecha);

                                                    // La fecha es correcta
                                                    if (vFechaCompleta[0].compare("") != 0) {
                                                        
                                                        // Creamos una tupla con la fecha del registro descargado
                                                        auto fechaRegistro = make_tuple(vFechaCompleta[0], vFechaCompleta[1]);

                                                        // Buscamos la fecha del registro descargado
                                                        if (Configuracion::vSolicitudes[nSolicitud].existeFechaCurva(vFechaCompleta[0], vFechaCompleta[1]) == true ) {
                                                            
                                                            // La curva ya existe
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLA CURVA YA EXISTE -> " + vFechaCompleta[0]);
                                                            // solicitud.setFechaSiguienteCurva(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[1]);
                                                            Configuracion::vSolicitudes[nSolicitud].setFechaSiguienteCurva(Fecha::siguienteFecha(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getTipoCurva()), "");

                                                            // vRegistros.clear();
                                                            if (vRegistros.empty() == false) {
                                                            
                                                                // Eliminamos el registro analizado
                                                                vRegistros.erase(vRegistros.begin(), vRegistros.begin() + tamanoRegistro);
                                                            
                                                            }

                                                        } else {

                                                            // La curva no existe
                                                            // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLA CURVA NO EXISTE -> " + vFechaCompleta[0]);
                                                            
                                                            // Comprobamos si la curva descargada tiene la fecha de la que seria la siguiente curva a descartar y no provocar huecos
                                                            if ( ((Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == 1)) || ( (this->reintento == 3) || (vFechaCompleta[0].compare(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == 0) )) {
                                                                
                                                                reintento = 0;

                                                                // Analizamos
                                                                int i = 1;
                                                                // VactE (kWh)
                                                                unsigned int vacte = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // CactE
                                                                unsigned int cacte = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                // Vacts(kWh)
                                                                unsigned int vacts = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // Cacts
                                                                unsigned int cacts = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                // Vrea1 (kVARh)
                                                                unsigned int vrea1 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // Crea1
                                                                unsigned int crea1 = vRegistros[i] & 0xFF;
                                                                i = i + 1;  
                                                                // Vrea2 (kVARh)
                                                                unsigned int vrea2 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // Crea2
                                                                unsigned int crea2 = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                // Vrea3 (kVARh)
                                                                unsigned int vrea3 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // Crea3
                                                                unsigned int crea3 = vRegistros[i] & 0xFF;
                                                                i = i + 1;    
                                                                // Vrea4 (kVARh)
                                                                unsigned int vrea4 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // Crea4
                                                                unsigned int crea4 = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                // R7
                                                                unsigned int r7 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // CR7
                                                                unsigned int cr7 = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                // R8
                                                                unsigned int r8 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                i = i + 4;
                                                                // CR8
                                                                unsigned int cr8 = vRegistros[i] & 0xFF;
                                                                i = i + 1;
                                                                
                                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCurva: [" + vFechaCompleta[0] + "]");
                                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Bandera: [" + vFechaCompleta[1] + "]");
                                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Entrante: [" + to_string(vacte) + "]");
                                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Saliente: [" + to_string(vacts) + "]");
                                                                if ((int)cacte != 0) {
                                                                    
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " [Codigo: " + to_string(cacte) + " -> " + obtenerError((int)cacte) + "]");

                                                                }
                                                                
                                                                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " ¿Fecha ok? [" + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + "]");
                                                                Archivo::escribir(Configuracion::vSolicitudes[nSolicitud].getNombreFicheroCSV() + ".csv", "\n" + vFechaCompleta[0] + ";" + vFechaCompleta[1] + ";"+to_string(vacte)+";"+to_string(cacte)+";"+to_string(vacts)+";"+to_string(cacts)+";"+to_string(vrea1)+";"+to_string(crea1)+";"+to_string(vrea2)+";"+to_string(crea2)+";"+to_string(vrea3)+";"+to_string(crea3)+";"+to_string(vrea4)+";"+to_string(crea4)+";"+to_string(r7)+";"+to_string(cr7)+";"+to_string(r8)+";"+to_string(cr8));
                                                                // Calculamos la fecha de la siguiente curva
                                                                Configuracion::vSolicitudes[nSolicitud].setFechaSiguienteCurva(Fecha::siguienteFecha(vFecha, Configuracion::vSolicitudes[nSolicitud].getTipoCurva()), "");
                                                                // Agregamos la fecha de la curva
                                                                Configuracion::vSolicitudes[nSolicitud].addFechaCurva(vFechaCompleta[0], vFechaCompleta[1]);
                                                                // Aumentamos el contador de registros descargados
                                                                Configuracion::vSolicitudes[nSolicitud].aumentaRegistrosDescargados();

                                                                //codRespuesta = 5;

                                                            } else {

                                                                // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nvFechaCompleta[0] -> " + vFechaCompleta[0] + " ¿==? " + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + " => " + to_string(Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0])));

                                                                //if (Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == -1) {

                                                                    // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actuales: " + to_string(Configuracion::vSolicitudes.size()));
                                                                    // for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) { 

                                                                    //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nN: [" + to_string(m) + "] Curva: [" + Configuracion::vSolicitudes[m].getTipoCurva() + "]" );
                                                                    //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modo: [" + Configuracion::vSolicitudes[m].getModoLectura() + "] Inicio: [" + Configuracion::vSolicitudes[m].getFechaInicio() + "]");
                                                                    //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Fin: [" + Configuracion::vSolicitudes[m].getFechaFin() + "] Ref: [" + to_string(Configuracion::vSolicitudes[m].getReferencia()) + "]");
                                                                    //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistros descargados: [" + to_string(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados()) + "]");
                                                                    
                                                                    // }

                                                                    this->reintento = this->reintento + 1;

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n\n###############################################################");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nATENCION HUECO !!! . Reintento: [" + to_string(this->reintento) + "/3]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFecha descargada [" + vFechaCompleta[0] + "] Bandera [" + vFechaCompleta[1] + "]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFecha que deberia tener: [" + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + "]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistros descargados: [" + to_string(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados()) + "]");
                                                                    
                                                                    vRegistros.clear();
                                                                    vTramaAnalizar.clear();
                                                                    vTramaRecibida.clear();

                                                                    // Añadir una nueva solicitud copiando los valores de la peticion, pero modificando la fecha de inicio
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nAñadimos una nueva solicitud...");
                                                                    // Codigo de respuesta
                                                                    solicitud.setCodigo(Configuracion::vSolicitudes[nSolicitud].getCodigo());
                                                                    // Mensaje de respuesta
                                                                    solicitud.setMensaje(Configuracion::vSolicitudes[nSolicitud].getMensaje());
                                                                    // Estampa de tiempo del comienzo de la llamada
                                                                    solicitud.setFechaInicioMinutaje(Configuracion::vSolicitudes[nSolicitud].getFechaInicioMinutaje());
                                                                    // Estampa de tiempo del final de la llamada
                                                                    solicitud.setFechaFinMinutaje(Configuracion::vSolicitudes[nSolicitud].getFechaInicioMinutaje());
                                                                    // Contador de registros descargados
                                                                    solicitud.setRegistrosDescargados(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados());
                                                                    // Tipo de curva
                                                                    solicitud.setTipoCurva(Configuracion::vSolicitudes[nSolicitud].getTipoCurva());
                                                                    // Modo de lectura
                                                                    solicitud.setModoLectura(Configuracion::vSolicitudes[nSolicitud].getModoLectura());
                                                                    // Fecha de inicio de la solicitud
                                                                    solicitud.setFechaInicio(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]); // Fecha de la curva que le toca descargar
                                                                    // Fecha de final de la solicitud
                                                                    solicitud.setFechaFin(Configuracion::vSolicitudes[nSolicitud].getFechaFin());
                                                                    // Calculamos la siguiente fecha a descargar
                                                                    solicitud.setFechaSiguienteCurva(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[1]);
                                                                    // Nombre del fichero CSV
                                                                    solicitud.setNombreFicheroCSV(Configuracion::vSolicitudes[nSolicitud].getNombreFicheroCSV());
                                                                    // Solicitud a la que hace referencia la solicitud de relleno
                                                                    if (Configuracion::vSolicitudes[nSolicitud].getReferencia() == -1) { 
                                                                    
                                                                        solicitud.setReferencia(nSolicitud);
                                                                    
                                                                    } else {

                                                                        solicitud.setReferencia(Configuracion::vSolicitudes[nSolicitud].getReferencia());

                                                                    }
                                                                    // Vector de tuplas donde se almacenaran todas las fechas de las curvas descargadas
                                                                    solicitud.setFechasCurvasDescargadas(Configuracion::vSolicitudes[nSolicitud].getFechasCurvasDescargadas());
                                                                    // Vector de tuplas donde se almacenaran todas las fechas de los cierres descargados
                                                                    solicitud.setFechasCierresDescargados(Configuracion::vSolicitudes[nSolicitud].getFechasCierresDescargados());
                                                                    
                                                                    // Agregamos la solicitud al vector de solicitudes
                                                                    vector<Solicitud> vSolicitudesAux;

                                                                    // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actuales: " + to_string(Configuracion::vSolicitudes.size()));
                                                                    for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) {

                                                                        vSolicitudesAux.push_back(Configuracion::vSolicitudes[m]);

                                                                        if (nSolicitud == m) {

                                                                            vSolicitudesAux.push_back(solicitud);

                                                                        }

                                                                    }

                                                                    Configuracion::vSolicitudes.clear();
                                                                    Configuracion::vSolicitudes = vSolicitudesAux;

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actualizadas: " + to_string(Configuracion::vSolicitudes.size()));
                                                                    for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) { 

                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nN: " + to_string(m) + " Curva: " + Configuracion::vSolicitudes[m].getTipoCurva() );
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modo: " + Configuracion::vSolicitudes[m].getModoLectura() + " Inicio: " + Configuracion::vSolicitudes[m].getFechaInicio() );
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Fin: " + Configuracion::vSolicitudes[m].getFechaFin() + " Ref: " + to_string(Configuracion::vSolicitudes[m].getReferencia()) );

                                                                    }
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################\n");
                                                                    // Dejamos por defecto la solicitud
                                                                    solicitud.setDefault();

                                                                    // Paramos de leer esta peticion
                                                                    codRespuesta = 7;

                                                                //}

                                                            }

                                                            if (vRegistros.empty() == false) {
                                                            
                                                                // Eliminamos el registro analizado
                                                                vRegistros.erase(vRegistros.begin(), vRegistros.begin() + tamanoRegistro);
                                                            
                                                            }

                                                        }

                                                        

                                                    } else {

                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################");
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nATENCION !!! . FECHA INCORRECTA !!!");
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################");
                                                        vRegistros.clear();
                                                    }

                                                } else {

                                                    // No se reconoce la DIRECCION DE OBJETO del registro
                                                    vRegistros.clear();
                                                    codRespuesta = 2;

                                                }

                                            }

                                            if ( (codRespuesta != 2) && (codRespuesta != 6) && (codRespuesta != 7) ) {
                                            
                                                // Trama devuelta OK
                                                codRespuesta = 5;

                                                // Cambiamos el FCB
                                                if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                    this->fija.FRAME_COUNT_BIT.set(0,1);

                                                } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                    this->fija.FRAME_COUNT_BIT.set(0,0);

                                                }

                                            }

                                            break;

                                        case 0x88: // REGISTROS DE CIERRES

                                            if ( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x05) ) {

                                                // Contrato 1
                                                if (vTramaAnalizar[12] == 0x86) {
                                                    contrato = 1;
                                                }
                                                // Contrato 2
                                                else if (vTramaAnalizar[12] == 0x87) {
                                                    contrato = 2;
                                                }
                                                // Contrato 3
                                                else if (vTramaAnalizar[12] == 0x88) {
                                                    contrato = 3;
                                                }

                                                // Eliminamos los 13 primeros bytes y los 2 ultimos
                                                vTramaAnalizar.erase(vTramaAnalizar.begin(), vTramaAnalizar.begin() + 13);
                                                vTramaAnalizar.erase(vTramaAnalizar.end() - 2, vTramaAnalizar.end());

                                                // Total de bytes que ocupa un registro
                                                tamanoRegistro = 63;

                                                // La trama nos llegara con este formato
                                                // [14] [0A 98 00 00] [5C 25 00 00] [10] [5E 08 00 00] [DA 01 00 00] [10] [A0 15 00 00] [00 06 00 00] [10] [00 00 00 00] [00] [00 00 00 00] [00] [23 00 00 00] [1E 07 4D 03 12] [10] [00 00 00 00] [10] [00 00 81 03 12] [00 80 E1 04 12] 
                                                for (int i = 0; i < vTramaAnalizar.size(); i++) {

                                                    // [14]
                                                    unsigned int tarifa = vTramaAnalizar[i] & 0xFF;
                                                    tarifa = tarifa - 20;
                                                    i = i + 1;
                                                    // [0A 98 00 00]
                                                    unsigned int absp = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [5C 25 00 00]
                                                    unsigned int incp = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [10]
                                                    unsigned int cp = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [5E 08 00 00]
                                                    unsigned int absql = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [DA 01 00 00]
                                                    unsigned int incql = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [10]
                                                    unsigned int cql = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [A0 15 00 00]
                                                    unsigned int absqc = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [00 06 00 00]
                                                    unsigned int incqc = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [10]
                                                    unsigned int cqc = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [00 00 00 00]
                                                    unsigned int r7 = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [00]
                                                    unsigned int cr7 = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [00 00 00 00]
                                                    unsigned int r8 = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;   
                                                    i = i + 4;
                                                    // [00]
                                                    unsigned int cr8 = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [23 00 00 00]
                                                    unsigned int vmaxdem = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;   
                                                    i = i + 4;

                                                    // [1E 07 4D 03 12]
                                                    // Fecha
                                                    // Montaremos la fecha con este formato [YYYY-MM-DD HH:MM:SS]
                                                    vector<unsigned char> tramaFecha;
                                                    for (int k = i; k < (i + 5); k++) {

                                                        tramaFecha.push_back(vTramaAnalizar[k]);

                                                    }

                                                    string fechaMaxDem = Fecha::fechaHexToFechaStr(tramaFecha)[0];

                                                    tramaFecha.clear();
                                                    i = i + 5;
                                                    if (fechaMaxDem.compare("") == 0) {

                                                        // La fecha no es correcta, se devuelve codigo de reenvio
                                                        codRespuesta = 2;
                                                        
                                                    }
                                                    // [10]
                                                    unsigned int cmaxdem = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [00 00 00 00]
                                                    unsigned int vexc = vTramaAnalizar[i] & 0xFF | vTramaAnalizar[i + 1] << 8 | vTramaAnalizar[i + 2] << 16 | vTramaAnalizar[i + 3] << 24;
                                                    i = i + 4;
                                                    // [10]
                                                    unsigned int cexc = vTramaAnalizar[i] & 0xFF;
                                                    i = i + 1;
                                                    // [00 00 81 03 12]
                                                    // Fecha
                                                    // Montaremos la fecha con este formato [YYYY-MM-DD HH:MM:SS]
                                                    for (int k = i; k < (i + 5); k++) {

                                                        tramaFecha.push_back(vTramaAnalizar[k]);

                                                    }

                                                    string fechaIni = Fecha::fechaHexToFechaStr(tramaFecha)[0];
                                                    tramaFecha.clear();
                                                    i = i + 5;
                                                    if (fechaIni.compare("") == 0) {

                                                        // La fecha no es correcta, se devuelve codigo de reenvio
                                                        codRespuesta = 2;
                                                        
                                                    }
                                                    // [00 80 E1 04 12]
                                                    // Fecha
                                                    // Montaremos la fecha con este formato [YYYY-MM-DD HH:MM:SS]
                                                    for (int k = i; k < (i + 5); k++) {

                                                        tramaFecha.push_back(vTramaAnalizar[k]);

                                                    }

                                                    string fechaFin = Fecha::fechaHexToFechaStr(tramaFecha)[0];
                                                    tramaFecha.clear();
                                                    i = i + 5;
                                                    if (fechaFin.compare("") == 0) {

                                                        // La fecha no es correcta, se devuelve codigo de reenvio
                                                        codRespuesta = 2;
                                                        
                                                    }

                                                    // Si el registro de CIERRE no existe
                                                    if (Configuracion::vSolicitudes[nSolicitud].existeFechaCierre(fechaIni, fechaFin, contrato, tarifa) == false) {

                                                        if (Configuracion::debug != 0) {

                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nInicio: [" + fechaIni + "]\tFin: [" + fechaFin + "]\tContrato: [" + to_string(contrato) + "]\tTarifa: [" + to_string(tarifa) + "]");

                                                        }

                                                        // Lo añadimos al vector de fechas
                                                        Configuracion::vSolicitudes[nSolicitud].addFechaCierre(fechaIni, fechaFin, contrato, (int)tarifa);
                                                        Archivo::escribir(Configuracion::vSolicitudes[nSolicitud].getNombreFicheroCSV() + ".csv", "\n" + fechaIni+";"+fechaFin+";"+to_string(contrato)+";"+to_string(tarifa)+";"+to_string(absp)+";"+to_string(incp)+";"+to_string(cp)+";"+to_string(absql)+";"+to_string(incql)+";"+to_string(cql)+";"+to_string(absqc)+";"+to_string(incqc)+";"+to_string(cqc)+";"+to_string(vmaxdem)+";"+fechaMaxDem+";"+to_string(cmaxdem)+";"+to_string(vexc)+";"+to_string(cexc));
                                                        Configuracion::vSolicitudes[nSolicitud].aumentaRegistrosDescargados();

                                                    } 
                                                    // Si el registro de CIERRE existe
                                                    else {

                                                        if (Configuracion::debug == 2) {
                                
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nInicio: [" + fechaIni + "]\tFin: [" + fechaFin + "]\tContrato: [" + to_string(contrato) + "]\tTarifa: [" + to_string(tarifa) + "] Esta curva YA EXISTE. No se inserta.");
                                                            
                                                        }

                                                        vTramaAnalizar.clear();

                                                    }

                                                }

                                                if (codRespuesta != 2) {
                                                
                                                    // Trama devuelta OK
                                                    codRespuesta = 5;

                                                    // Cambiamos el FCB
                                                    if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                        this->fija.FRAME_COUNT_BIT.set(0,1);

                                                    } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                        this->fija.FRAME_COUNT_BIT.set(0,0);

                                                    }

                                                }

                                            }

                                            break;

                                        case 0xBB: // RESPUESTA A FINALIZAR SESION

                                            if (causaTramaAnalizar == 0x07) {
                                    
                                                // Trama devuelta OK
                                                codRespuesta = 1;
                                                vTramaAnalizar.clear();
                                                vTramaRecibida.clear();

                                            } else {

                                                // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                                codRespuesta = 2;
                                                vTramaAnalizar.clear();
                                                                                    
                                            }

                                            break;

                                        
                                        
                                        default:

                                            // IDENTIFICADOR DE TIPO de la trama a analizar desconocido ¿?
                                            // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                            codRespuesta = 2;
                                            vTramaAnalizar.clear();

                                            break;


                                    }

                                } else { // Recibimos una trama FIJA

                                    codRespuesta = 2;

                                    // // Obtenemos el CAMPO DE CONTROL de la trama FIJA a analizar
                                    // unsigned int ccTramaAnalizar = vTramaAnalizar[1];


                                    // switch (ccTramaAnalizar)
                                    // {
                                    //     case 0x09: // DESACTIVACION CONFIRMADA
                                            
                                    //         // Dejar de solicitar datos.
                                    //         //codRespuesta = 6;

                                    //         codRespuesta = 2;

                                    //         break;
                                    
                                    //     default:

                                    //         // Se ha recibido una trama, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama.
                                    //         codRespuesta = 2;  

                                    //         break;
                                    // }

                                    // Limpiamos la trama
                                    vTramaAnalizar.clear();

                                }

                                break;

                            default:

                                // Campo de Control enviado desconocido ¿?

                                break;
                        }

                    } else if (flagTramaEnviada == this->variable.FLAG_TRAMA_VARIABLE) { // Si hemos enviado una trama VARIABLE

                        // Obtenemos el IDENTIFICADOR DE TIPO de la trama VARIABLE enviada
                        unsigned char idTipoTramaEnviada = vTramaEnviada[7];
                        // Vector de IDENTIFICADORES de TIPOS que enviamos
                        vector<unsigned char> idTipos{ 0xB7, 0xBE, 0xBD, 0x86, 0xBB }; 
                        // Buscamos en el vector de IDENTIFICADORES DE TIPOS el que hemos enviado
                        auto it = search(idTipos.begin(), idTipos.end(), vTramaEnviada.begin() + 7, vTramaEnviada.begin() + 7);
                        if (it != idTipos.end()) {

                            // Comprobamos que hemos recibido una trama FIJA
                            if (flagTramaAnalizar == this->fija.FLAG_TRAMA_FIJA) {

                                if (vTramaAnalizar[1] == 0x00) {

                                    // INICIO SESION Y ENVIO CLAVE
                                    if (idTipoTramaEnviada == 0xB7) {

                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nComprobando parametros del contador...");
                                    
                                    } else if ( (idTipoTramaEnviada == 0xBE) || (idTipoTramaEnviada == 0xBD) || (idTipoTramaEnviada == 0x86) ) { // SOLICITUDES DE CURVAS
                                        
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitud de registros...");

                                    } else if ( (idTipoTramaEnviada == 0xBB) && (nSolicitud == 0) ) { // FINALIZACION DE SESION

                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFinalizacion de sesion...");

                                        // Cambiamos el FCB
                                        if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                            this->fija.FRAME_COUNT_BIT.set(0,1);

                                        } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                            this->fija.FRAME_COUNT_BIT.set(0,0);

                                        }

                                    }

                                    // Trama devuelta OK
                                    codRespuesta = 1;

                                    vTramaAnalizar.clear();
                                    vTramaRecibida.clear();    

                                } else {

                                    // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                    codRespuesta = 2;
                                    vTramaAnalizar.clear();
                                                                        
                                }

                            } else if (flagTramaAnalizar == this->variable.FLAG_TRAMA_VARIABLE) {

                                // Obtenemos el IDENTIFICADOR DE TIPO de la trama VARIABLE enviada
                                unsigned char idTipoTramaEnviada = vTramaEnviada[7];
                                // Vector de IDENTIFICADORES de TIPOS que enviamos
                                vector<unsigned char> idTipos{ 0xB7, 0xBE, 0xBD, 0x86, 0xBB }; 
                                // Buscamos en el vector de IDENTIFICADORES DE TIPOS el que hemos enviado
                                auto it = search(idTipos.begin(), idTipos.end(), vTramaEnviada.begin() + 7, vTramaEnviada.begin() + 7);
                                if (it != idTipos.end()) {

                                    if ( (idTipoTramaEnviada == 0xBB) && (vTramaAnalizar[0] != this->fija.FLAG_TRAMA_FIJA) ) {

                                        codRespuesta = 2;
                                        vTramaAnalizar.clear();

                                    } else {
                                        // Obtenemos el CAMPO DE CONTROL de la trama VARIABLE a analizar
                                        unsigned int ccTramaAnalizar = vTramaAnalizar[4];
                                        // Obtenemos el IDENTIFICADOR DE TIPO de la trama VARIABLE a analizar
                                        unsigned int idTipoTramaAnalizar = vTramaAnalizar[7];
                                        // Obtenemos la CAUSA de la trama VARIABLE a analizar
                                        unsigned int causaTramaAnalizar = vTramaAnalizar[9];
                                        // Vector que almacenara los registros recibidos
                                        vector<unsigned char> vRegistros;
                                        // Vector que almacenara la fecha recogida de la trama en hexadecimal
                                        vector<unsigned char> vFecha;
                                        // Vector que almacenara la fecha y la bandera 
                                        vector<string> vFechaCompleta;
                                        // Tamaño del registro
                                        int tamanoRegistro = 0;

                                        // Comprobamos el IDENTIFICADOR DE TIPO de la trama VARIABLE a analizar
                                        switch (idTipoTramaAnalizar)
                                        {
                                            case 0xB7: // RESPUESTA AL INICIO SESION Y ENVIO CLAVE

                                                // Confirmacion de activacion ACEPTADA
                                                if ( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x07) ) {
                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConfirmacion de activacion ACEPTADA...");

                                                    codRespuesta = 1;
                                                    vTramaAnalizar.clear();
                                                    vTramaRecibida.clear();
                                                    

                                                } else if ( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x47) ) { // Confirmacion de activacion RECHAZADA
                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nERROR en el Punto Medida o Clave...");

                                                    // Error en el Punto de Medida o la Clave
                                                    codRespuesta = 4;
                                                    vTramaAnalizar.clear();
                                                    vTramaRecibida.clear();

                                                } else if( (ccTramaAnalizar == 0x08) && (causaTramaAnalizar == 0x10) ) { // Finalizacion de la activacion
                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFinalizacion de la activacion...");

                                                    // Finalizacion de la activacion
                                                    codRespuesta = 4;
                                                    vTramaAnalizar.clear();
                                                    vTramaRecibida.clear();

                                                }

                                                break;

                                            case 0xBE: // RESPUESTA A LA SOLICITUD DE TOTALES INTEGRADOS INCREMENTALES
                                            case 0xBD: // RESPUESTA A LA SOLICITUD DE TOTALES INTEGRADOS ABSOLUTOS
                                            case 0x86: // RESPUESTA A LA SOLICITUD DE CIERRES
                                    
                                                // Comprobamos que hemos enviado una trama FIJA de SOLICITUD DE DATOS
                                                if ( (vTramaEnviada[1] == 0x5B) || (vTramaEnviada[1] == 0x7B) ) {

                                                    if (ccTramaAnalizar == 0x08) {

                                                        if (causaTramaAnalizar == 0x07) { // Datos de usuario RESPOND y CONFIRMACION

                                                            // Trama devuelta OK
                                                            codRespuesta = 5;

                                                            // Cambiamos el FCB
                                                            if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                                this->fija.FRAME_COUNT_BIT.set(0,1);

                                                            } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                                this->fija.FRAME_COUNT_BIT.set(0,0);

                                                            }

                                                            vTramaAnalizar.clear();
                                                        
                                                        } else {
                                                            
                                                            // Dejar de solicitar datos
                                                            codRespuesta = 6;

                                                            // El contador ya ha devuelto todos los registros, no tiene mas
                                                            if (causaTramaAnalizar == 0x0A) {

                                                                // if (Configuracion::debug == 2) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nEl contador no tiene mas registros entre las fechas solicitadas");
                                                                
                                                                // }

                                                            } else if (causaTramaAnalizar == 0x47) { // Datos solicitados no disponibles

                                                                // this->result.setCodigo((solicitud + 1), 14);
                                                                // this->result.addMensaje((solicitud + 1), "Datos solicitados no disponibles. ");

                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nDatos solicitados no disponibles. ");
                                                                
                                                                // }

                                                            } else if (causaTramaAnalizar == 0x12) { // Periodo de integracion no disponible

                                                                // this->result.setCodigo((solicitud + 1), 7);
                                                                // this->result.addMensaje((solicitud + 1), "Periodo de integracion no disponible. ");

                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nPeriodo de integracion no disponible. ");
                                                                
                                                                // }
                                                            } else if (causaTramaAnalizar == 0x0D) { // Registro de datos solicitado no disponible
                                                                
                                                                // this->result.setCodigo((solicitud + 1), 6);
                                                                // this->result.addMensaje((solicitud + 1), "Registro de datos solicitado no disponible. ");
                
                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistro de datos solicitado no disponible. ");
                                                                
                                                                // }
                                                            } else if (causaTramaAnalizar == 0x0E) { // Tipo de ASDU solicitado no disponible

                                                                // this->result.setCodigo((solicitud + 1), 9);
                                                                // this->result.addMensaje((solicitud + 1), "No dispone de registros " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de registros " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                // }
                                                            } else if (causaTramaAnalizar == 0x0F) { // Nº de registro en el ASDU enviado por CM desconocido

                                                                // this->result.setCodigo((solicitud + 1), 10);
                                                                // this->result.addMensaje((solicitud + 1), "No dispone de " + Configuracion::vSolicitudes[nSolicitud].getTipoCurva() + " de " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                            
                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de " + Configuracion::vSolicitudes[nSolicitud].getTipoCurva() + " de " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                // }
                                                            } else if (causaTramaAnalizar == 0x10) { // Especificacion de direccion en el ASDU enviado por CM desconocida

                                                                // this->result.setCodigo((solicitud + 1), 11);
                                                                // this->result.addMensaje((solicitud + 1), "No dispone de Totales Integrados CON reservas de " + Configuracion::vSolicitudes[nSolicitud].getTipoCurva() + " de " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNo dispone de Totales Integrados CON reservas de " + Configuracion::vSolicitudes[nSolicitud].getTipoCurva() + " de " + Configuracion::vSolicitudes[nSolicitud].getModoLectura() + ". ");
                                                                
                                                                // }
                                                            } else if (causaTramaAnalizar == 0x11) { // Datos solicitados no disponibles

                                                                // this->result.setCodigo((solicitud + 1), 12);
                                                                // this->result.addMensaje((solicitud + 1), "Objeto de informacion no disponible.");
                                                                
                                                                // if (Configuracion::debug != 0) {

                                                                //     Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nObjeto de informacion no disponible. ");
                                                                
                                                                // }
                                                            } else {

                                                                // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                                                codRespuesta = 2;
                                                                vTramaAnalizar.clear();

                                                            }

                                                        }

                                                    }

                                                }
                                                
                                                break;

                                            
                                            case 0x8C: // REGISTROS DE TOTALES INTEGRADOS INCREMENTALES
                                            case 0x8B: // REGISTROS DE TOTALES INTEGRADOS ABSOLUTOS

                                                // No nos importa el tipo de trama enviada, la trama recibida
                                                // es de registros integrados (curvas), obtenemos la informacion

                                                // Eliminamos los 13 primeros bytes y los 2 ultimos
                                                copy((vTramaAnalizar.begin() + 13), (vTramaAnalizar.end() - 2), back_inserter(vRegistros));
                                                
                                                // Una vez extraida la informacion de la trama a analizar, la eliminamos
                                                vTramaAnalizar.clear();
                                                //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nvRegistros..." + to_string(vRegistros.size()));
                                                
                                                while (vRegistros.empty() == false) {

                                                    vFecha.clear();
                                                    vFechaCompleta.clear();

                                                    // El primer BYTE nos dice que tipo de DIRECCION DE OBJETO es
                                                    switch (vRegistros[0])
                                                    {
                                                        case 0x09: // Totales Integrados Generales Con Reservas
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Generales Con Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                            tamanoRegistro = 46;

                                                            break;
                                                        
                                                        case 0x0A: // Totales Integrados Generales Sin Reservas
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Generales Sin Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                            tamanoRegistro = 36;

                                                            break;
                                                        
                                                        case 0x0B: // Totales Integrados Consumo Puro Sin Reservas
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTotales Integrados Consumo Puro Sin Reservas [" + to_string((int)vRegistros[0]) + "]");
                                                            tamanoRegistro = 21;

                                                            break;
                                                    
                                                        default:
                                                            
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n####################### No se reconoce la DIRECCION DE OBJETO del registro [" + to_string((int)vRegistros[0]) + "]");
                                                            tamanoRegistro = 0;
                                                    
                                                            break;
                                                    }

                                                    if (tamanoRegistro != 0) {

                                                        // Obtenemos la fecha del registro
                                                        for (int i = (tamanoRegistro - 5); i < tamanoRegistro; i++) {

                                                            vFecha.push_back(vRegistros[i]);

                                                        }

                                                        stringstream ss;
                                                        ss  << "\n[REGISTRO] [" + to_string(tamanoRegistro) + "] <- ";
                                                        for (int i = 0; i < tamanoRegistro; i++) {

                                                            ss << "[" << i << "]";
                                                            ss << hex << setw(2) << setfill('0') << (unsigned int)vRegistros[i];
                                                            ss << dec << "(" << (unsigned int)vRegistros[i] << ") ";

                                                        }
                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", ss.str());
                                                        
                                                        // Comprobamos que la fecha es correcta
                                                        vFechaCompleta = Fecha::fechaHexToFechaStr(vFecha);

                                                        // La fecha es correcta
                                                        if (vFechaCompleta[0].compare("") != 0) {
                                                            
                                                            // Creamos una tupla con la fecha del registro descargado
                                                            auto fechaRegistro = make_tuple(vFechaCompleta[0], vFechaCompleta[1]);

                                                            // Buscamos la fecha del registro descargado
                                                            if (Configuracion::vSolicitudes[nSolicitud].existeFechaCurva(vFechaCompleta[0], vFechaCompleta[1]) == true ) {
                                                                
                                                                // La curva ya existe
                                                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLA CURVA YA EXISTE -> " + vFechaCompleta[0]);
                                                                // solicitud.setFechaSiguienteCurva(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[1]);
                                                                Configuracion::vSolicitudes[nSolicitud].setFechaSiguienteCurva(Fecha::siguienteFecha(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getTipoCurva()), "");

                                                                // vRegistros.clear();
                                                                if (vRegistros.empty() == false) {
                                                                
                                                                    // Eliminamos el registro analizado
                                                                    vRegistros.erase(vRegistros.begin(), vRegistros.begin() + tamanoRegistro);
                                                                
                                                                }

                                                            } else {

                                                                // La curva no existe
                                                                // Comprobamos si la curva descargada tiene la fecha de la que seria la siguiente curva a descartar y no provocar huecos
                                                                if ( ((Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == 1)) || ( (this->reintento == 3) || (vFechaCompleta[0].compare(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == 0) )) {
                                                                    
                                                                    reintento = 0;

                                                                    // Comprobamos que tipo de registro nos devuelve para extraer la informacion
                                                                    // if () {

                                                                    // } else if () {

                                                                    // } else if () {
                                                                        
                                                                    // }

                                                                    // Analizamos
                                                                    int i = 1;
                                                                    // VactE (kWh)
                                                                    unsigned int vacte = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // CactE
                                                                    unsigned int cacte = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    // Vacts(kWh)
                                                                    unsigned int vacts = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // Cacts
                                                                    unsigned int cacts = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    // Vrea1 (kVARh)
                                                                    unsigned int vrea1 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // Crea1
                                                                    unsigned int crea1 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;  
                                                                    // Vrea2 (kVARh)
                                                                    unsigned int vrea2 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // Crea2
                                                                    unsigned int crea2 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    // Vrea3 (kVARh)
                                                                    unsigned int vrea3 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // Crea3
                                                                    unsigned int crea3 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;    
                                                                    // Vrea4 (kVARh)
                                                                    unsigned int vrea4 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // Crea4
                                                                    unsigned int crea4 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    // R7
                                                                    unsigned int r7 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // CR7
                                                                    unsigned int cr7 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    // R8
                                                                    unsigned int r8 = vRegistros[i] & 0xFF | vRegistros[i + 1] << 8 | vRegistros[i + 2] << 16 | vRegistros[i + 3] << 24;
                                                                    i = i + 4;
                                                                    // CR8
                                                                    unsigned int cr8 = vRegistros[i] & 0xFF;
                                                                    i = i + 1;
                                                                    
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCurva: [" + vFechaCompleta[0] + "]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Bandera: [" + vFechaCompleta[1] + "]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Entrante: [" + to_string(vacte) + "]");
                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Saliente: [" + to_string(vacts) + "]");
                                                                    if ((int)cacte != 0) {
                                                                        
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " [Codigo: " + to_string(cacte) + " -> " + obtenerError((int)cacte) + "]");

                                                                    }
                                                                    
                                                                    // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " ¿Fecha ok? [" + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + "]");
                                                                    Archivo::escribir(Configuracion::vSolicitudes[nSolicitud].getNombreFicheroCSV() + ".csv", "\n" + vFechaCompleta[0] + ";" + vFechaCompleta[1] + ";"+to_string(vacte)+";"+to_string(cacte)+";"+to_string(vacts)+";"+to_string(cacts)+";"+to_string(vrea1)+";"+to_string(crea1)+";"+to_string(vrea2)+";"+to_string(crea2)+";"+to_string(vrea3)+";"+to_string(crea3)+";"+to_string(vrea4)+";"+to_string(crea4)+";"+to_string(r7)+";"+to_string(cr7)+";"+to_string(r8)+";"+to_string(cr8));
                                                                    // Calculamos la fecha de la siguiente curva
                                                                    Configuracion::vSolicitudes[nSolicitud].setFechaSiguienteCurva(Fecha::siguienteFecha(vFecha, Configuracion::vSolicitudes[nSolicitud].getTipoCurva()), "");
                                                                    // Agregamos la fecha de la curva
                                                                    Configuracion::vSolicitudes[nSolicitud].addFechaCurva(vFechaCompleta[0], vFechaCompleta[1]);
                                                                    // Aumentamos el contador de registros descargados
                                                                    Configuracion::vSolicitudes[nSolicitud].aumentaRegistrosDescargados();

                                                                    //codRespuesta = 6;

                                                                } else {

                                                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nvFechaCompleta[0] -> " + vFechaCompleta[0] + " ¿==? " + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + " => " + to_string(Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0])));

                                                                    //if (Fecha::comparaFechas(vFechaCompleta[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]) == 1) {

                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actuales: " + to_string(Configuracion::vSolicitudes.size()));
                                                                        for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) { 

                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nN: [" + to_string(m) + "] Curva: [" + Configuracion::vSolicitudes[m].getTipoCurva() + "]" );
                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modo: [" + Configuracion::vSolicitudes[m].getModoLectura() + "] Inicio: [" + Configuracion::vSolicitudes[m].getFechaInicio() + "]");
                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Fin: [" + Configuracion::vSolicitudes[m].getFechaFin() + "] Ref: [" + to_string(Configuracion::vSolicitudes[m].getReferencia()) + "]");
                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistros descargados: [" + to_string(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados()) + "]");
                                                                        
                                                                        }

                                                                        this->reintento = this->reintento + 1;

                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n\n###############################################################");
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nATENCION HUECO !!! . Reintento: [" + to_string(this->reintento) + "/3]");
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFecha descargada [" + vFechaCompleta[0] + "] Bandera [" + vFechaCompleta[1] + "]");
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFecha que deberia tener: [" + Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0] + "]");
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nRegistros descargados: [" + to_string(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados()) + "]");
                                                                        
                                                                        vRegistros.clear();
                                                                        vTramaAnalizar.clear();
                                                                        vTramaRecibida.clear();

                                                                        // Añadir una nueva solicitud copiando los valores de la peticion, pero modificando la fecha de inicio
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nAñadimos una nueva solicitud...");
                                                                        // Codigo de respuesta
                                                                        solicitud.setCodigo(Configuracion::vSolicitudes[nSolicitud].getCodigo());
                                                                        // Mensaje de respuesta
                                                                        solicitud.setMensaje(Configuracion::vSolicitudes[nSolicitud].getMensaje());
                                                                        // Estampa de tiempo del comienzo de la llamada
                                                                        solicitud.setFechaInicioMinutaje(Configuracion::vSolicitudes[nSolicitud].getFechaInicioMinutaje());
                                                                        // Estampa de tiempo del final de la llamada
                                                                        solicitud.setFechaFinMinutaje(Configuracion::vSolicitudes[nSolicitud].getFechaInicioMinutaje());
                                                                        // Contador de registros descargados
                                                                        solicitud.setRegistrosDescargados(Configuracion::vSolicitudes[nSolicitud].getRegistrosDescargados());
                                                                        // Tipo de curva
                                                                        solicitud.setTipoCurva(Configuracion::vSolicitudes[nSolicitud].getTipoCurva());
                                                                        // Modo de lectura
                                                                        solicitud.setModoLectura(Configuracion::vSolicitudes[nSolicitud].getModoLectura());
                                                                        // Fecha de inicio de la solicitud
                                                                        solicitud.setFechaInicio(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0]); // Fecha de la curva que le toca descargar
                                                                        // Fecha de final de la solicitud
                                                                        solicitud.setFechaFin(Configuracion::vSolicitudes[nSolicitud].getFechaFin());
                                                                        // Calculamos la siguiente fecha a descargar
                                                                        solicitud.setFechaSiguienteCurva(Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[0], Configuracion::vSolicitudes[nSolicitud].getFechaSiguienteCurva()[1]);
                                                                        // Nombre del fichero CSV
                                                                        solicitud.setNombreFicheroCSV(Configuracion::vSolicitudes[nSolicitud].getNombreFicheroCSV());
                                                                        // Solicitud a la que hace referencia la solicitud de relleno
                                                                        if (Configuracion::vSolicitudes[nSolicitud].getReferencia() == -1) { 
                                                                        
                                                                            solicitud.setReferencia(nSolicitud);
                                                                        
                                                                        } else {

                                                                            solicitud.setReferencia(Configuracion::vSolicitudes[nSolicitud].getReferencia());

                                                                        }
                                                                        // Vector de tuplas donde se almacenaran todas las fechas de las curvas descargadas
                                                                        solicitud.setFechasCurvasDescargadas(Configuracion::vSolicitudes[nSolicitud].getFechasCurvasDescargadas());
                                                                        // Vector de tuplas donde se almacenaran todas las fechas de los cierres descargados
                                                                        solicitud.setFechasCierresDescargados(Configuracion::vSolicitudes[nSolicitud].getFechasCierresDescargados());
                                                                        
                                                                        // Agregamos la solicitud al vector de solicitudes
                                                                        vector<Solicitud> vSolicitudesAux;

                                                                        // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actuales: " + to_string(Configuracion::vSolicitudes.size()));
                                                                        for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) {

                                                                            vSolicitudesAux.push_back(Configuracion::vSolicitudes[m]);

                                                                            if (nSolicitud == m) {

                                                                                vSolicitudesAux.push_back(solicitud);

                                                                            }

                                                                        }

                                                                        Configuracion::vSolicitudes.clear();
                                                                        Configuracion::vSolicitudes = vSolicitudesAux;

                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nSolicitudes actualizadas: " + to_string(Configuracion::vSolicitudes.size()));
                                                                        for (int m = 0; m < Configuracion::vSolicitudes.size(); m++) { 

                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nN: " + to_string(m) + " Curva: " + Configuracion::vSolicitudes[m].getTipoCurva() );
                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modo: " + Configuracion::vSolicitudes[m].getModoLectura() + " Inicio: " + Configuracion::vSolicitudes[m].getFechaInicio() );
                                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Fin: " + Configuracion::vSolicitudes[m].getFechaFin() + " Ref: " + to_string(Configuracion::vSolicitudes[m].getReferencia()) );

                                                                        }
                                                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################\n");
                                                                        // Dejamos por defecto la solicitud
                                                                        solicitud.setDefault();

                                                                        // Paramos de leer esta peticion
                                                                        codRespuesta = 7;

                                                                    //}
                                                                }

                                                                if (vRegistros.empty() == false) {
                                                                
                                                                    // Eliminamos el registro analizado
                                                                    vRegistros.erase(vRegistros.begin(), vRegistros.begin() + tamanoRegistro);
                                                                
                                                                }

                                                            }

                                                            

                                                        } else {

                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################");
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nATENCION !!! . FECHA INCORRECTA !!!");
                                                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n###############################################################");
                                                            vRegistros.clear();
                                                        }

                                                    } else {

                                                        // No se reconoce la DIRECCION DE OBJETO del registro
                                                        vRegistros.clear();
                                                        codRespuesta = 2;

                                                    }

                                                }

                                                if ( (codRespuesta != 2) && (codRespuesta != 6) && (codRespuesta != 7) ) {
                                                
                                                    // Trama devuelta OK
                                                    codRespuesta = 5;

                                                    // Cambiamos el FCB
                                                    if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                        this->fija.FRAME_COUNT_BIT.set(0,1);

                                                    } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                        this->fija.FRAME_COUNT_BIT.set(0,0);

                                                    }

                                                }

                                                break;

                                            case 0x88: // REGISTROS DE CIERRES

                                                // No nos importa el tipo de trama enviada, la trama recibida
                                                // es de registros integrados (curvas), obtenemos la informacion

                                                

                                                // Extraemos las curvas
                                                // Procesamos curva a curva y la guardamos en el .CSV



                                                // Trama devuelta OK
                                                codRespuesta = 5;

                                                // Cambiamos el FCB
                                                if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                                                    this->fija.FRAME_COUNT_BIT.set(0,1);

                                                } else if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 1) {

                                                    this->fija.FRAME_COUNT_BIT.set(0,0);

                                                }

                                                break;

                                            
                                            case 0xBB: // RESPUESTA A FINALIZAR SESION

                                                if (causaTramaAnalizar == 0x07) {
                                        
                                                    // Trama devuelta OK
                                                    codRespuesta = 1;
                                                    vTramaAnalizar.clear();
                                                    vTramaRecibida.clear();

                                                } else {

                                                    // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                                    codRespuesta = 2;
                                                    vTramaAnalizar.clear();
                                                                                        
                                                }

                                                break;

                                            default:

                                                // IDENTIFICADOR DE TIPO de la trama a analizar desconocido ¿?
                                                codRespuesta = 2;
                                                vTramaAnalizar.clear();

                                                break;
                                        }

                                    }

                                }

                            } else {

                                // Trama devuelta, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama
                                codRespuesta = 2;
                                vTramaAnalizar.clear();

                            }


                        } else {

                            // Identificador de Tipo enviado desconocido ¿?
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nIDENTIFICADOR DE TIPO enviado DESCONOCIDO...");

                        }

                    }

                } else {

                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nLa trama no tiene el formato correcto...");
                    vTramaAnalizar.clear();

                }

            } else {

                // Se ha recibido una trama, pero no es lo que se espera recibir. Se vuelve a solicitar la ultima trama.
                codRespuesta = 2;
                vTramaRecibida.clear();

            }

        }

    } else {

        // No se ha recibido ninguna trama. Se realiza un reintento
        codRespuesta = 3;

    }

    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: " + to_string(codRespuesta) + "\n\n");
    
    return codRespuesta;

}

// Metodo que realizara la descarga de informacion del contador
void IEC::descarga() {

    // Vector que almacenara enteros que corresponden a cada una de las tramas de solicitud de activacion
    vector<int> vActivacion{0, 1, 2, 3};
    // Vector que almacenara enteros que corresponden a cada una de las tramas de solicitud de activacion
    vector<int> vFinalizacion{0, 1};
    // Trama que enviaremos
    vector<unsigned char> vTrama;
    // Codigo devuelto por el metodo que analizara la trama
    int codigo = -1;
    // Contador de reintentos utilizados
    int reintentos = 0;
    // Contador de reenvios utilizados
    int reenvios = 0;
    // Flag que nos dira si seguimos enviando tramas
    bool leer = true;
    // Flag que nos dira si si enviamos la siguiente trama
    bool siguiente = false;

    if (this->comunicacion.abrir() == true) {

        if (this->comunicacion.conectar() == true && this->comunicacion.llamar() == true) {

            // Activacion de la sesion con el contador
            for (int i = 0; (i < vActivacion.size()) && (leer == true); i++) {

                // Obtenemos la trama a enviar
                if (vActivacion[i] == 0) {

                    // Solicitud Estado Enlace
                    vTrama = this->fija.solicitudEstadoEnlace();

                    if (Configuracion::debug != 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nEnlazando con el contador...");

                    } 
                
                } else if (vActivacion[i] == 1) {

                    // Reposicion Enlace Remoto
                    vTrama = this->fija.reposicionEnlaceRemoto();

                } else if (vActivacion[i] == 2) {

                    // Inicio Sesion y Envio Clave
                    vTrama = this->variable.C_AC_NA_2();

                } else if (vActivacion[i] == 3) {

                    // Solicitud Datos
                    vTrama = this->fija.solicitudDatos();

                }

                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;
                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);
                    
                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(0, this->comunicacion.recibir(), vTrama);

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                    } else if (leer == false) {

                        // Solicitud Estado Enlace
                        if (vActivacion[i] == 0) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {
                            
                                
                                Configuracion::vSolicitudes[j].setCodigo(4);
                                Configuracion::vSolicitudes[j].setMensaje("El contador no enlaza, reintentar o revisar direccion de enlace.");

                            }

                        } 
                        // Reposicion Enlace Remoto
                        else if (vActivacion[i] == 1) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(15);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion antes de validar la Clave y el Punto de Medida.");

                            }

                        } 
                        // Inicio Sesion y Envio Clave
                        else if (vActivacion[i] == 2) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(16);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion esperando respuesta de validacion de la Clave y el Punto de Medida.");

                            }

                        }
                        // Solicitud Datos
                        else if (vActivacion[i] == 3) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(17);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion despues de validacion de la Clave y el Punto de Medida y antes de la descarga.");

                            }

                        }
                        

                    }

                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                // Reseteamos los flags y contadores
                reintentos = 0;
                reenvios = 0;
                codigo = -1;
                siguiente = false;
                this->comunicacion.setTimeout(reintentos);

            }

            // Solicitud de informacion
            for (int j = 0; (j < Configuracion::vSolicitudes.size()) && (leer == true); j++) {
                
                

                if (Configuracion::vSolicitudes[j].getModoLectura().compare("absoluta") == 0) {

                    if (Configuracion::vSolicitudes[j].getTipoCurva().compare("TM1") == 0) {
                        
                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CURVA ABSOLUTA HORARIA desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());

                    } else if (Configuracion::vSolicitudes[j].getTipoCurva().compare("TM2") == 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CURVA ABSOLUTA CUARTA-HORARIA desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());

                    }

                    vTrama = this->variable.C_CB_NT_2(j);

                    // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                    do {

                        // Envio de la trama
                        this->comunicacion.enviar(vTrama);

                        // Recepcion y analisis de la trama recibida
                        codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                        // Aumentamos el contador de reenvios
                        if (codigo == 2) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                            }

                            reenvios = reenvios + 1;

                        }

                        // Aumentamos el contador de reintentos
                        if (codigo == 3) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                            }

                            reintentos = reintentos + 1;
                            this->comunicacion.setTimeout(reintentos);

                        }

                        // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                        if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                            leer = false;

                        }

                        // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                        if (codigo == 1) {

                            leer = true;
                            siguiente = true;

                            reintentos = 0;
                            reenvios = 0;

                        } else if (leer == false) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(13);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                            
                            }

                        }

                        if (codigo == 6) {

                            siguiente = true;

                        }

                        if (codigo == 5) {
                            
                            this->comunicacion.setTimeout(0);

                            reintentos = 0;
                            reenvios = 0;
                        }

                    
                    } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                    // Reseteamos los flags y contadores
                    if (codigo != 1) {
                        
                        this->comunicacion.setTimeout(0);

                    }
                    reintentos = 0;
                    reenvios = 0;
                    codigo = -1;
                    siguiente = false;
                    

                    // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                    do {

                        // Aumentamos el contador de reenvios
                        if (codigo == 2) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                            }

                            reenvios = reenvios + 1;
                        }

                        // Aumentamos el contador de reintentos
                        if (codigo == 3) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                            }

                            reintentos = reintentos + 1;
                            this->comunicacion.setTimeout(reintentos);

                        }

                        // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                        if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                            leer = false;

                        }

                        // Solicitud Datos
                        vTrama = this->fija.solicitudDatos();

                        // Envio de la trama
                        this->comunicacion.enviar(vTrama);
                        
                        // Recepcion y analisis de la trama recibida
                        codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                        // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                        if (codigo == 1) {

                            leer = true;
                            siguiente = true;

                            reintentos = 0;
                            reenvios = 0;

                        } else if (leer == false) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(13);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                            
                            }

                        }

                        if (codigo == 5) {

                            this->comunicacion.setTimeout(0);

                            reintentos = 0;
                            reenvios = 0;

                        }

                        if (codigo == 6) {

                            siguiente = true;
                            //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                        }
                    
                    } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                    // Reseteamos los flags y contadores
                    if (codigo != 5) {
                        
                        this->comunicacion.setTimeout(0);

                    }
                    reintentos = 0;
                    reenvios = 0;
                    codigo = -1;
                    siguiente = false;

                } else if (Configuracion::vSolicitudes[j].getModoLectura().compare("incremental") == 0) {

                    if (Configuracion::vSolicitudes[j].getTipoCurva().compare("TM1") == 0) {
                        
                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CURVA INCREMENTAL HORARIA desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());

                    } else if (Configuracion::vSolicitudes[j].getTipoCurva().compare("TM2") == 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CURVA INCREMENTAL CUARTA-HORARIA desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());
                        
                    }
                    
                    vTrama = this->variable.C_CB_NU_2(j);

                    // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                    do {

                        // Aumentamos el contador de reenvios
                        if (codigo == 2) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                            }

                            reenvios = reenvios + 1;
                        }

                        // Aumentamos el contador de reintentos
                        if (codigo == 3) {

                            if (Configuracion::debug != 0) {
                                
                                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                            }

                            reintentos = reintentos + 1;
                            this->comunicacion.setTimeout(reintentos);

                        }

                        // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                        if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                            leer = false;

                        }

                        // Envio de la trama
                        this->comunicacion.enviar(vTrama);
                        
                        // Recepcion y analisis de la trama recibida
                        codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                        // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                        if (codigo == 1) {

                            leer = true;
                            siguiente = true;

                            reintentos = 0;
                            reenvios = 0;

                        } else if (leer == false) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int z = j; z < Configuracion::vSolicitudes.size(); z++) {

                                Configuracion::vSolicitudes[z].setCodigo(13);
                                Configuracion::vSolicitudes[z].setMensaje("Corte de comunicacion realizando la peticion de curvas.");
                            
                            }

                        }

                        if (codigo == 5) {

                            this->comunicacion.setTimeout(0);

                            reintentos = 0;
                            reenvios = 0;

                        }

                        if ( (codigo == 5) || (codigo == 6) || (codigo == 7) ) {

                            siguiente = true;
                            //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                        }
                    
                    } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                    // Reseteamos los flags y contadores
                    if (codigo != 5) {
                        
                        this->comunicacion.setTimeout(0);

                    }
                    
                    reintentos = 0;
                    reenvios = 0;
                    
                    if (codigo != 7) {

                        codigo = -1;

                    }
                    
                    siguiente = false;
                    this->fija.FRAME_COUNT_BIT.set(0,0);
                    
                   

                    if ( (leer == true) && ((codigo != 7)) ) {

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;
                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Solicitud Datos
                            vTrama = this->fija.solicitudDatos();

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);
                            
                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 5) {

                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;

                            }

                            if ( (codigo == 6) || (codigo == 7) ) {

                                siguiente = true;
                                //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                            }
                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 5) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;

                    }

                } else if (Configuracion::vSolicitudes[j].getTipoCurva().compare("C1") == 0) {

                    if (Configuracion::vSolicitudes[j].getModoLectura().compare("contratoT") == 0) {

                        for (int c = 1; (c < 4) && (leer == true); c++) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CIERRES CONTRATO " + to_string(c) + " desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());

                            vTrama = this->variable.C_TA_VM_2(j, c);

                            // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                            do {

                                // Envio de la trama
                                this->comunicacion.enviar(vTrama);

                                // Recepcion y analisis de la trama recibida
                                codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                                // Aumentamos el contador de reenvios
                                if (codigo == 2) {

                                    if (Configuracion::debug != 0) {
                                        
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                    }

                                    reenvios = reenvios + 1;

                                }

                                // Aumentamos el contador de reintentos
                                if (codigo == 3) {

                                    if (Configuracion::debug != 0) {
                                        
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                    }

                                    reintentos = reintentos + 1;
                                    this->comunicacion.setTimeout(reintentos);

                                }

                                // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                                if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                    leer = false;

                                }

                                // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                                if (codigo == 1) {

                                    leer = true;
                                    siguiente = true;

                                    reintentos = 0;
                                    reenvios = 0;

                                } else if (leer == false) {

                                    // Aplicamos el codigo y el mensaje de error a las solicitudes
                                    for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                        Configuracion::vSolicitudes[j].setCodigo(13);
                                        Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                    
                                    }

                                }

                                if (codigo == 6) {

                                    siguiente = true;

                                }

                                if (codigo == 5) {
                                    
                                    this->comunicacion.setTimeout(0);

                                    reintentos = 0;
                                    reenvios = 0;
                                }

                            
                            } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                            // Reseteamos los flags y contadores
                            if (codigo != 1) {
                                
                                this->comunicacion.setTimeout(0);

                            }
                            reintentos = 0;
                            reenvios = 0;
                            codigo = -1;
                            siguiente = false;
                            

                            // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                            do {

                                // Aumentamos el contador de reenvios
                                if (codigo == 2) {

                                    if (Configuracion::debug != 0) {
                                        
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                    }

                                    reenvios = reenvios + 1;
                                }

                                // Aumentamos el contador de reintentos
                                if (codigo == 3) {

                                    if (Configuracion::debug != 0) {
                                        
                                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                    }

                                    reintentos = reintentos + 1;
                                    this->comunicacion.setTimeout(reintentos);

                                }

                                // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                                if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                    leer = false;

                                }

                                // Solicitud Datos
                                vTrama = this->fija.solicitudDatos();

                                // Envio de la trama
                                this->comunicacion.enviar(vTrama);
                                
                                // Recepcion y analisis de la trama recibida
                                codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                                // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                                if (codigo == 1) {

                                    leer = true;
                                    siguiente = true;

                                    reintentos = 0;
                                    reenvios = 0;

                                } else if (leer == false) {

                                    // Aplicamos el codigo y el mensaje de error a las solicitudes
                                    for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                        Configuracion::vSolicitudes[j].setCodigo(13);
                                        Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                    
                                    }

                                }

                                if (codigo == 5) {

                                    this->comunicacion.setTimeout(0);

                                    reintentos = 0;
                                    reenvios = 0;

                                }

                                if (codigo == 6) {

                                    siguiente = true;
                                    //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                                }
                            
                            } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                            // Reseteamos los flags y contadores
                            if (codigo != 5) {
                                
                                this->comunicacion.setTimeout(0);

                            }
                            reintentos = 0;
                            reenvios = 0;
                            codigo = -1;
                            siguiente = false;
                        
                        }

                    } else if (Configuracion::vSolicitudes[j].getModoLectura().compare("contrato1") == 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CIERRES CONTRATO 1 desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());
                        
                        vTrama = this->variable.C_TA_VM_2(j, 1);

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);

                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;

                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 6) {

                                siguiente = true;

                            }

                            if (codigo == 5) {
                                
                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;
                            }

                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 1) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                        

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;
                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Solicitud Datos
                            vTrama = this->fija.solicitudDatos();

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);
                            
                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 5) {

                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;

                            }

                            if (codigo == 6) {

                                siguiente = true;
                                //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                            }
                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 5) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                        
                    } else if (Configuracion::vSolicitudes[j].getModoLectura().compare("contrato2") == 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CIERRES CONTRATO 2 desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());
                        
                        vTrama = this->variable.C_TA_VM_2(j, 2);

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);

                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;

                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 6) {

                                siguiente = true;

                            }

                            if (codigo == 5) {
                                
                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;
                            }

                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 1) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                        

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;
                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Solicitud Datos
                            vTrama = this->fija.solicitudDatos();

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);
                            
                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 5) {

                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;

                            }

                            if (codigo == 6) {

                                siguiente = true;
                                //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                            }
                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 5) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                    
                    } else if (Configuracion::vSolicitudes[j].getModoLectura().compare("contrato3") == 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[" + to_string(j) + "] Solicitando CIERRES CONTRATO 3 desde " + Configuracion::vSolicitudes[j].getFechaInicio() + " hasta " + Configuracion::vSolicitudes[j].getFechaFin());
                        
                        vTrama = this->variable.C_TA_VM_2(j, 3);

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);

                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;

                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 6) {

                                siguiente = true;

                            }

                            if (codigo == 5) {
                                
                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;
                            }

                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 1) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                        

                        // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                        do {

                            // Aumentamos el contador de reenvios
                            if (codigo == 2) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                                }

                                reenvios = reenvios + 1;
                            }

                            // Aumentamos el contador de reintentos
                            if (codigo == 3) {

                                if (Configuracion::debug != 0) {
                                    
                                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                                }

                                reintentos = reintentos + 1;
                                this->comunicacion.setTimeout(reintentos);

                            }

                            // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                            if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                                leer = false;

                            }

                            // Solicitud Datos
                            vTrama = this->fija.solicitudDatos();

                            // Envio de la trama
                            this->comunicacion.enviar(vTrama);
                            
                            // Recepcion y analisis de la trama recibida
                            codigo = this->analizar(j, this->comunicacion.recibir(), vTrama);

                            // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                            if (codigo == 1) {

                                leer = true;
                                siguiente = true;

                                reintentos = 0;
                                reenvios = 0;

                            } else if (leer == false) {

                                // Aplicamos el codigo y el mensaje de error a las solicitudes
                                for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                    Configuracion::vSolicitudes[j].setCodigo(13);
                                    Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                                
                                }

                            }

                            if (codigo == 5) {

                                this->comunicacion.setTimeout(0);

                                reintentos = 0;
                                reenvios = 0;

                            }

                            if (codigo == 6) {

                                siguiente = true;
                                //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                            }
                        
                        } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                        // Reseteamos los flags y contadores
                        if (codigo != 5) {
                            
                            this->comunicacion.setTimeout(0);

                        }
                        reintentos = 0;
                        reenvios = 0;
                        codigo = -1;
                        siguiente = false;
                    
                    }

                }

                    // Cambiamos el FCB
                    // if ((int)(this->fija.FRAME_COUNT_BIT.to_ulong()) == 0) {

                    //     this->fija.FRAME_COUNT_BIT.set(0,1);

                    // } else 

            }

            // Finalizacion de la sesion con el contador
            for (int k = 0; (k < vFinalizacion.size()) && (leer == true); k++) {

                // Obtenemos la trama a enviar
                if (vFinalizacion[k] == 0) {

                    // Finalizar Sesion
                    vTrama = this->variable.C_FS_NA_2();
                
                } else if (vFinalizacion[k] == 1) {

                    // Solicitud Datos
                    vTrama = this->fija.solicitudDatos();

                }
                
                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;
                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);
                    
                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(-1, this->comunicacion.recibir(), vTrama);

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                    } else if (leer == false) {

                        // Aplicamos el codigo y el mensaje de error a las solicitudes
                        // for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                        //     Configuracion::vSolicitudes[j].setCodigo(13);
                        //     Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                        
                        // }

                    }

                
                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );
                

            }

            this->comunicacion.colgar();

        }

        // Cerramos la comunicacion
        this->comunicacion.cerrar();
        
    }

    // Comprobamos si existen peticiones para evitar huecos y si las hay actualizamos las solicitudes a las que hacen referencia
    int contador = 0;
    for (int h = (Configuracion::vSolicitudes.size() - 1); h >= 0; h--) {

        if (Configuracion::vSolicitudes[h].getReferencia() != -1) {

            // Actualizamos la informacion en la solicitud principal
            Configuracion::vSolicitudes[Configuracion::vSolicitudes[h].getReferencia()].setRegistrosDescargados(Configuracion::vSolicitudes[h].getRegistrosDescargados());

            h = Configuracion::vSolicitudes[h].getReferencia();

        }

        if (Configuracion::vSolicitudes[h].getReferencia() == -1) {

            contador = contador + 1;

        }

    }


    /*
        MENSAJES DE ERROR
    */

    // 0 = Sin Errores, Todo Ok. V
    // 1 = Módem llamante no responde. V
    // 2 = El módem remoto no responde (GSM). V
    // 3 = El módem remoto no responde (IP). V
    // 4 = El contador no enlaza, reintentar o revisar dirección de enlace. V
    // 5 = Error en el Punto de Medida o Clave. V
    // 6 = Registro de datos solicitados no disponibles. V
    // 7 = Periodo de integración No disponible (falta de registros o de tipo de curva por diferentes motivos posibles). V
    // 8 = Parámetros de Entrada Inválido. V
    // 9 = No dispone de registros {Absolutos | Incrementales | Cierre}. V
    // 10 = No dispone de {TM1 | TM2 | C1 | C2 | C3} de {Absolutos | Incrementales | Cierre}. V
    // 11 = No dispone de {Totales integrados con reservas | Totales integrados sin reservas} de {TM1 | TM2 | C1 | C2 | C3} de {Absolutos | Incrementales | Cierre}. V
    // 12 = Objeto de informacion no disponible. V
    // 13 = Corte de comunicación durante la descarga. V
    // 14 = Datos solicitados no diponibles. V
    // 15 = Corte de comunicacion antes de validar la Clave y el Punto de Medida.
    // 16 = Corte de comunicacion esperando respuesta de validacion de la Clave y el Punto de Medida.
    // 17 = Corte de comunicacion despues de validacion de la Clave y el Punto de Medida y antes de la descarga.
    // 18 = Corte de comunicacion realizando la peticion de curvas

    int contadorAux = 0;
    string strResultado = "";
    string strInformacion = "";
    if (Configuracion::debug != 0) {

        strInformacion += "\n";
        strInformacion += "#################################################################################################\n";
        strInformacion += "##\n";
        strInformacion += "##\tRESULTADOS\n";
        strInformacion += "#################################################################################################\n";

    }
    strResultado += "[";
    // Mostramos los resultados
    for (int h = 0; h < Configuracion::vSolicitudes.size(); h++) {

        if (Configuracion::vSolicitudes[h].getReferencia() == -1) {

            contadorAux = contadorAux + 1;

            strResultado += "{";
            strResultado += "\"codigo\": \"" + to_string(Configuracion::vSolicitudes[h].getCodigo()) + "\",";
            strResultado += "\"mensaje\": \"" + Configuracion::vSolicitudes[h].getMensaje() + "\",";
            strResultado += "\"fechaIniMinutaje\": \"" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "\",";
            strResultado += "\"fechaFinMinutaje\": \"" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "\",";
            strResultado += "\"fichero\": \"" + Configuracion::vSolicitudes[h].getNombreFicheroCSV() + "\",";
            strResultado += "\"registros\": \"" + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\"";
            if (contadorAux == contador) {

                strResultado += "}";

            } else {

                strResultado += "},";

            }

            if (Configuracion::debug != 0) {

                if (Configuracion::vSolicitudes[h].getTipoCurva().compare("TM1") == 0) {
                
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("incremental") == 0) {

                        strInformacion += "##\tCURVA HORARIA INCREMENTAL DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("absoluta") == 0) {

                        strInformacion += "##\tCURVA HORARIA ABSOLUTA DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } 

                } else if (Configuracion::vSolicitudes[h].getTipoCurva().compare("TM2") == 0) {
                    
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("incremental") == 0) {

                        strInformacion += "##\tCURVA CUARTA-HORARIA INCREMENTAL DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("absoluta") == 0) {

                        strInformacion += "##\tCURVA CUARTA-HORARIA ABSOLUTA DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } 

                } else if (Configuracion::vSolicitudes[h].getTipoCurva().compare("C1") == 0) {
                    
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("contratoT") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 1, CONTRATO 2 Y CONTRATO 3 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato1") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 1 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato2") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 2 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato3") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 3 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    }

                }

            }

        }

    }
    strResultado += "]";

    if (Configuracion::debug != 0) {

        strInformacion += "#################################################################################################\n";
        strInformacion += "\n";

    }

    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", strInformacion);
    cout << strResultado;

}

// Metodo que leera la configuracion del equipo
void IEC::configuracion() {

}

// Metodo que reiniciara el modem
void IEC::reinicio() {

}

// Metodo que descargara los instantaneos
void IEC::instantaneos() {

}

// Metodo que comprobara el estado del suministro, ademas de validar los parametros
void IEC::comprobar() {

}

// Metodo que devolvera la Fecha completa del contador
void IEC::hora() {

    // Vector que almacenara enteros que corresponden a cada una de las tramas de solicitud de activacion
    vector<int> vActivacion{0, 1, 2, 3};
    // Vector que almacenara enteros que corresponden a cada una de las tramas de solicitud de activacion
    vector<int> vFinalizacion{0, 1};
    // Trama que enviaremos
    vector<unsigned char> vTrama;
    // Codigo devuelto por el metodo que analizara la trama
    int codigo = -1;
    // Contador de reintentos utilizados
    int reintentos = 0;
    // Contador de reenvios utilizados
    int reenvios = 0;
    // Flag que nos dira si seguimos enviando tramas
    bool leer = true;
    // Flag que nos dira si si enviamos la siguiente trama
    bool siguiente = false;

    if (this->comunicacion.abrir() == true) {

        if (this->comunicacion.conectar() == true && this->comunicacion.llamar() == true) {

            // Activacion de la sesion con el contador
            for (int i = 0; (i < vActivacion.size()) && (leer == true); i++) {

                // Obtenemos la trama a enviar
                if (vActivacion[i] == 0) {

                    // Solicitud Estado Enlace
                    vTrama = this->fija.solicitudEstadoEnlace();

                    if (Configuracion::debug != 0) {

                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nEnlazando con el contador...");

                    } 
                
                } else if (vActivacion[i] == 1) {

                    // Reposicion Enlace Remoto
                    vTrama = this->fija.reposicionEnlaceRemoto();

                } else if (vActivacion[i] == 2) {

                    // Inicio Sesion y Envio Clave
                    vTrama = this->variable.C_AC_NA_2();

                } else if (vActivacion[i] == 3) {

                    // Solicitud Datos
                    vTrama = this->fija.solicitudDatos();

                }

                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;
                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);
                    
                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(0, this->comunicacion.recibir(), vTrama);

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                    } else if (leer == false) {

                        // Solicitud Estado Enlace
                        if (vActivacion[i] == 0) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {
                            
                                
                                Configuracion::vSolicitudes[j].setCodigo(4);
                                Configuracion::vSolicitudes[j].setMensaje("El contador no enlaza, reintentar o revisar direccion de enlace.");

                            }

                        } 
                        // Reposicion Enlace Remoto
                        else if (vActivacion[i] == 1) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(15);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion antes de validar la Clave y el Punto de Medida.");

                            }

                        } 
                        // Inicio Sesion y Envio Clave
                        else if (vActivacion[i] == 2) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(16);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion esperando respuesta de validacion de la Clave y el Punto de Medida.");

                            }

                        }
                        // Solicitud Datos
                        else if (vActivacion[i] == 3) {

                            // Aplicamos el codigo y el mensaje de error a las solicitudes
                            for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                                Configuracion::vSolicitudes[j].setCodigo(17);
                                Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion despues de validacion de la Clave y el Punto de Medida y antes de la descarga.");

                            }

                        }
                        

                    }

                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                // Reseteamos los flags y contadores
                reintentos = 0;
                reenvios = 0;
                codigo = -1;
                siguiente = false;
                this->comunicacion.setTimeout(reintentos);

            }

            if (leer == true) {

                // Solicitud de informacion
                vTrama = this->variable.C_TI_NA_2();

                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);

                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(0, this->comunicacion.recibir(), vTrama);

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {
                            
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;

                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {
                            
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                        reintentos = 0;
                        reenvios = 0;

                    } else if (leer == false) {

                        // Aplicamos el codigo y el mensaje de error a las solicitudes
                        for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                            Configuracion::vSolicitudes[j].setCodigo(13);
                            Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                        
                        }

                    }

                    if (codigo == 6) {

                        siguiente = true;

                    }

                    if (codigo == 5) {
                        
                        this->comunicacion.setTimeout(0);

                        reintentos = 0;
                        reenvios = 0;
                    }

                
                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                // Reseteamos los flags y contadores
                if (codigo != 1) {
                    
                    this->comunicacion.setTimeout(0);

                }
                reintentos = 0;
                reenvios = 0;
                codigo = -1;
                siguiente = false;
            
            }

            if (leer == true) {

                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {
                            
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;
                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {
                            
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Solicitud Datos
                    vTrama = this->fija.solicitudDatos();

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);
                    
                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(0, this->comunicacion.recibir(), vTrama);

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                        reintentos = 0;
                        reenvios = 0;

                    } else if (leer == false) {

                        // Aplicamos el codigo y el mensaje de error a las solicitudes
                        for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                            Configuracion::vSolicitudes[j].setCodigo(13);
                            Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                        
                        }

                    }

                    if (codigo == 5) {

                        this->comunicacion.setTimeout(0);

                        reintentos = 0;
                        reenvios = 0;

                    }

                    if (codigo == 6) {

                        siguiente = true;
                        //Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nCodigo: 6. Paramos de leer.");
                    }
                
                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );

                // Reseteamos los flags y contadores
                if (codigo != 5) {
                    
                    this->comunicacion.setTimeout(0);

                }
                reintentos = 0;
                reenvios = 0;
                codigo = -1;
                siguiente = false;

            }

            // Finalizacion de la sesion con el contador
            for (int k = 0; (k < vFinalizacion.size()) && (leer == true); k++) {

                // Obtenemos la trama a enviar
                if (vFinalizacion[k] == 0) {

                    // Finalizar Sesion
                    vTrama = this->variable.C_FS_NA_2();
                
                } else if (vFinalizacion[k] == 1) {

                    // Solicitud Datos
                    vTrama = this->fija.solicitudDatos();

                }
                
                // Realizar el envio y la recepcion hasta recibir una trama conocida o agotar los reintentos
                do {

                    // Aumentamos el contador de reenvios
                    if (codigo == 2) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReenvio: " + to_string(reenvios + 1) + "/5");

                        }

                        reenvios = reenvios + 1;
                    }

                    // Aumentamos el contador de reintentos
                    if (codigo == 3) {

                        if (Configuracion::debug != 0) {

                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nReintento: " + to_string(reintentos + 1) + "/5");

                        }

                        reintentos = reintentos + 1;
                        this->comunicacion.setTimeout(reintentos);

                    }

                    // Si hemos llegado al maximo de reintentos o de reenvios o el codigo devuelto es 4, activamos el flag para dejar de leer
                    if ( (codigo == 4) || (reintentos == 5) || (reenvios == 5) ) {

                        leer = false;

                    }

                    // Envio de la trama
                    this->comunicacion.enviar(vTrama);
                    
                    // Recepcion y analisis de la trama recibida
                    codigo = this->analizar(-1, this->comunicacion.recibir(), vTrama);

                    // Si el codigo es de trama OK, activamos el flag de leer a TRUE
                    if (codigo == 1) {

                        leer = true;
                        siguiente = true;

                    } else if (leer == false) {

                        // Aplicamos el codigo y el mensaje de error a las solicitudes
                        // for (int j = 0; j < Configuracion::vSolicitudes.size(); j++) {

                        //     Configuracion::vSolicitudes[j].setCodigo(13);
                        //     Configuracion::vSolicitudes[j].setMensaje("Corte de comunicacion durante la descarga.");
                        
                        // }

                    }

                
                } while ( (siguiente == false) && (leer == true) && (reintentos < 5) && (reenvios < 5) );
                

            }

            this->comunicacion.colgar();

        }

        // Cerramos la comunicacion
        this->comunicacion.cerrar();
        
    }

    // Comprobamos si existen peticiones para evitar huecos y si las hay actualizamos las solicitudes a las que hacen referencia
    int contador = 0;
    for (int h = (Configuracion::vSolicitudes.size() - 1); h >= 0; h--) {

        if (Configuracion::vSolicitudes[h].getReferencia() != -1) {

            // Actualizamos la informacion en la solicitud principal
            Configuracion::vSolicitudes[Configuracion::vSolicitudes[h].getReferencia()].setRegistrosDescargados(Configuracion::vSolicitudes[h].getRegistrosDescargados());

            h = Configuracion::vSolicitudes[h].getReferencia();

        }

        if (Configuracion::vSolicitudes[h].getReferencia() == -1) {

            contador = contador + 1;

        }

    }


    /*
        MENSAJES DE ERROR
    */

    // 0 = Sin Errores, Todo Ok. V
    // 1 = Módem llamante no responde. V
    // 2 = El módem remoto no responde (GSM). V
    // 3 = El módem remoto no responde (IP). V
    // 4 = El contador no enlaza, reintentar o revisar dirección de enlace. V
    // 5 = Error en el Punto de Medida o Clave. V
    // 6 = Registro de datos solicitados no disponibles. V
    // 7 = Periodo de integración No disponible (falta de registros o de tipo de curva por diferentes motivos posibles). V
    // 8 = Parámetros de Entrada Inválido. V
    // 9 = No dispone de registros {Absolutos | Incrementales | Cierre}. V
    // 10 = No dispone de {TM1 | TM2 | C1 | C2 | C3} de {Absolutos | Incrementales | Cierre}. V
    // 11 = No dispone de {Totales integrados con reservas | Totales integrados sin reservas} de {TM1 | TM2 | C1 | C2 | C3} de {Absolutos | Incrementales | Cierre}. V
    // 12 = Objeto de informacion no disponible. V
    // 13 = Corte de comunicación durante la descarga. V
    // 14 = Datos solicitados no diponibles. V
    // 15 = Corte de comunicacion antes de validar la Clave y el Punto de Medida.
    // 16 = Corte de comunicacion esperando respuesta de validacion de la Clave y el Punto de Medida.
    // 17 = Corte de comunicacion despues de validacion de la Clave y el Punto de Medida y antes de la descarga.
    // 18 = Corte de comunicacion realizando la peticion de curvas

    int contadorAux = 0;
    string strResultado = "";
    string strInformacion = "";
    if (Configuracion::debug != 0) {

        strInformacion += "\n";
        strInformacion += "#################################################################################################\n";
        strInformacion += "##\n";
        strInformacion += "##\tRESULTADOS\n";
        strInformacion += "#################################################################################################\n";

    }
    strResultado += "[";
    // Mostramos los resultados
    for (int h = 0; h < Configuracion::vSolicitudes.size(); h++) {

        if (Configuracion::vSolicitudes[h].getReferencia() == -1) {

            contadorAux = contadorAux + 1;

            strResultado += "{";
            strResultado += "\"codigo\": \"" + to_string(Configuracion::vSolicitudes[h].getCodigo()) + "\",";
            strResultado += "\"mensaje\": \"" + Configuracion::vSolicitudes[h].getMensaje() + "\",";
            strResultado += "\"fechaIniMinutaje\": \"" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "\",";
            strResultado += "\"fechaFinMinutaje\": \"" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "\",";
            strResultado += "\"fichero\": \"" + Configuracion::vSolicitudes[h].getNombreFicheroCSV() + "\",";
            strResultado += "\"registros\": \"" + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\"";
            if (contadorAux == contador) {

                strResultado += "}";

            } else {

                strResultado += "},";

            }

            if (Configuracion::debug != 0) {

                if (Configuracion::vSolicitudes[h].getTipoCurva().compare("TM1") == 0) {
                
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("incremental") == 0) {

                        strInformacion += "##\tCURVA HORARIA INCREMENTAL DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("absoluta") == 0) {

                        strInformacion += "##\tCURVA HORARIA ABSOLUTA DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } 

                } else if (Configuracion::vSolicitudes[h].getTipoCurva().compare("TM2") == 0) {
                    
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("incremental") == 0) {

                        strInformacion += "##\tCURVA CUARTA-HORARIA INCREMENTAL DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("absoluta") == 0) {

                        strInformacion += "##\tCURVA CUARTA-HORARIA ABSOLUTA DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } 

                } else if (Configuracion::vSolicitudes[h].getTipoCurva().compare("C1") == 0) {
                    
                    if (Configuracion::vSolicitudes[h].getModoLectura().compare("contratoT") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 1, CONTRATO 2 Y CONTRATO 3 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato1") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 1 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato2") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 2 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    } else if (Configuracion::vSolicitudes[h].getModoLectura().compare("contrato3") == 0) {

                        strInformacion += "##\tCIERRE CONTRATO 3 DESDE [" + Configuracion::vSolicitudes[h].getFechaInicio() + "] HASTA [" + Configuracion::vSolicitudes[h].getFechaFin() + "]\n";

                        strInformacion += "##\tMENSAJE: " + Configuracion::vSolicitudes[h].getMensaje() + "\n";
                        strInformacion += "##\tFECHA INICIO MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaInicioMinutaje() + "]\t\tFECHA FINAL MINUTAJE: [" + Configuracion::vSolicitudes[h].getFechaFinMinutaje() + "]\n";
                        strInformacion += "##\tREGISTROS DESCARGADOS: " + to_string(Configuracion::vSolicitudes[h].getRegistrosDescargados()) + "\n";
                        strInformacion += "##\n";

                    }

                }

            }

        }

    }
    strResultado += "]";

    if (Configuracion::debug != 0) {

        strInformacion += "#################################################################################################\n";
        strInformacion += "\n";

    }

    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", strInformacion);
    cout << strResultado;

}

string IEC::obtenerError(int codError) {
    string strError = "";
    string binario = "";
    stringstream ss;

    ss << hex << codError;
    unsigned n;
    ss >> n;
    bitset<8> b(n);
    binario = b.to_string();
    
    for (int i = 0; i < 8; i++) {
        
        if (binario[i] == '1') {

            switch (i)
            {
                case 0:

                    strError += "Lectura invalida, ";

                    break;

                case 1:

                    strError += "Contador sincronizado durante el periodo, ";
                
                    break;

                case 2:

                    strError += "Overflow, ";

                    break;

                case 3:

                    strError += "Verificacion horaria durante el periodo, ";

                    break;

                case 4:

                    strError += "Modificacion de parametros durante el periodo, ";

                    break;

                case 5:

                    strError += "Se produjo un intrusismo durante el periodo, ";

                    break;

                case 6:

                    strError += "Periodo incompleto por fallo de alimentacion en el periodo, ";

                    break;

                case 7:

                    strError += "Reserva, ";

                    break;

                default:
                    break;
            }

        }

    }

    if (codError != 0) {
    
        strError = strError.substr(0, strError.size() - 2);
    
    }

    return strError;
}


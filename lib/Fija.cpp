#include "../include/Fija.h"

// FLAGS
const unsigned char Fija::FLAG_TRAMA_FIJA   = 0x10;
const unsigned char Fija::FLAG_FIN = 0x16;
const int           Fija::TAMANO_TRAMA_FIJA = 6;


// CAMPOS DE CONTROL DEL MAESTRO
const string Fija::SOLICITUD_ESTADO_ENLACE  = "01001001";   // 0x49
const string Fija::REPOSICION_ENLACE_REMOTO = "01000000";   // 0x40
const string Fija::SOLICITUD_DATOS          = "01X11011";   // {0x5B | 0x7B} El bit X es el acuse de recibo, que variara

bitset<1> Fija::FRAME_COUNT_BIT;

Fija::Fija() {

    // Ponemos el BIT FRAME COUNT a 0 por defecto
    FRAME_COUNT_BIT.set(0, 0);

}

vector<unsigned char> Fija::solicitudEstadoEnlace() {
    vector<unsigned char> trama;
    bitset<8> campoControlC;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_FIJA);

    // Campo de Control C
    try {

        for (int i = 0; i < SOLICITUD_ESTADO_ENLACE.size(); i++) {

            campoControlC.set( ((SOLICITUD_ESTADO_ENLACE.size() - 1) - i) , (SOLICITUD_ESTADO_ENLACE[i] - 48) );

        }

    } catch (exception& e) {

        // cout << "[ERROR] [Fija::solicitudEstadoEnlace()] Ha ocurrido un error seteando el Campo de Control. Exception: " << e.what() << "\n";
    
    }
    trama.push_back(strBinToHex(campoControlC.to_string()));

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    if (Configuracion::debug == 2) {
        
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n#############################");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n## Solicitud Estado Enlace ##");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n#############################");
    
    }

    return trama;
}

vector<unsigned char> Fija::reposicionEnlaceRemoto() {
    vector<unsigned char> trama;
    bitset<8> campoControlC;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_FIJA);

    // Campo de Control C
    try {

        for (int i = 0; i < REPOSICION_ENLACE_REMOTO.size(); i++) {
            campoControlC.set( ((REPOSICION_ENLACE_REMOTO.size() - 1) - i) , (REPOSICION_ENLACE_REMOTO[i] - 48) );
        }
        
    } catch (exception& e) {
        // cout << "[ERROR] [Fija::Fija()] Ha ocurrido un error seteando el Campo de Control. Exception: " << e.what() << "\n";
    }
    trama.push_back(strBinToHex(campoControlC.to_string()));

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    if (Configuracion::debug == 2) {
    
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n##############################");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n## Reposicion Enlace Remoto ##");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n##############################");
    
    }

    return trama;
}

vector<unsigned char> Fija::solicitudDatos() {
    vector<unsigned char> trama;
    bitset<8> campoControlC;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_FIJA);

    // Campo de Control C
    try {

        for (int i = 0; i < SOLICITUD_DATOS.size(); i++) {
            if (i == 2) { // FRAME BIT COUNT (FBC)
                campoControlC.set( ((SOLICITUD_DATOS.size() - 1) - i) , (int)(this->FRAME_COUNT_BIT.to_ulong()) );
            } else {
                campoControlC.set( ((SOLICITUD_DATOS.size() - 1) - i) , (SOLICITUD_DATOS[i] - 48) );
            }
        }
        
    } catch (exception& e) {
        // cout << "[ERROR] [Fija::Fija()] Ha ocurrido un error seteando el Campo de Control. Exception: " << e.what() << "\n";
    }
    trama.push_back(strBinToHex(campoControlC.to_string()));

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);
    
    if (Configuracion::debug == 2) {
    
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n########################");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n## Solicitud de Datos ##");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n########################");
    
    } 

    return trama;
}

// Metodo para convertir un entero a hexadecimal
unsigned char Fija::intToHex(int numero) {
    stringstream stream;

    stream << hex << numero;
    // Convertimos a unsigned char el valor
    union U
    {
        unsigned int valor;
        unsigned char componente;
    };
    U u;

    stringstream SS(stream.str());
    SS >> hex >> u.valor;
    
    return u.componente;
}

// Metodo para convertir un numeero binario en string a hexadecimal
int Fija::strBinToHex(string strBinario) {
    bitset<8> set(strBinario);	
    std::stringstream ss;
    int numero;

    ss << hex << set.to_ulong() << endl;
    ss >> numero;

    return numero;
}

// Calculo del checksum de la trama
unsigned char Fija::getChecksum(vector<unsigned char> trama) {
    unsigned char cheksum = 0x00;

    // Si la trama es de tipo fija el calculo del checksum empieza en la posicion 2
    int posicion = 2;

    // Byte con la suma de todos los bytes comenzando por el campo de control (incluido) hasta el cheksum (no incluido)
    int suma = 0;
    for (int i = (posicion - 1); i < trama.size(); i++) {

        // Sumamos todos los bytes desde la posicion hasta el final
        suma = suma + (int)trama[i];

    }
    cheksum = intToHex(suma);

    return cheksum;
}

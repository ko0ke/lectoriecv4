#include "../include/Comunicacion.h"

// Constructor por defecto
Comunicacion::Comunicacion() { }

// Metodo que abrira el Socket/Puerto Serie
bool Comunicacion::abrir() {

    bool creado = true;

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {


        // Inicializamos WinSock
        if (WSAStartup(MAKEWORD(2,2), &wsaData) != 0) {

            // cout << "[ERROR] [WSAStartup] Error inicializando WinSock.\n";
            creado = false;

        } else {
        
            // Abrimos el socket
            // AF_INET -> Familia TCP/IP
            // SOCK_STREAM -> Socket de flujo
            // 0 -> Seleccion de protocolo automatica (TCP es el protocolo usado por defecto para SOCK_STREAM
            //      Cuando se usa UDP, por defecto es SOCK_DGRAM
            if((this->fdSocket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {

                creado = false;

            }
        
            // Aplicamos un timeout al socket
            this->setTimeout(0);

            if (Configuracion::debug != 0) {

                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConectando con el modem remoto (IP)...");

            }
        
        }

    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        if (Configuracion::debug != 0) {

            // Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConectando con el modem remoto (GSM)...");

        }

        // Abrimos el puerto COM
        hPuerto = CreateFile((char*)Configuracion::puertoCOM.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

        // Si no podemos abrir el puerto, añadimos el codigo y el mensaje de error
        if (hPuerto == INVALID_HANDLE_VALUE) {
    
            for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                
                Configuracion::vSolicitudes[i].setCodigo(1);
                Configuracion::vSolicitudes[i].setMensaje("Modem llamante no responde.");
                
            }

            if (Configuracion::debug != 0) {
        
                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem llamante no responde (GSM).");
            
            }
            
            creado = false;

        }

    }

    return creado;

}

// Metodo que realizara la conexion entre los dos puntos
bool Comunicacion::conectar() {

    bool conectado = true;

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {

        // Aplicamos la IP y el Puerto para la conexion
        this->stSocket.sin_addr.s_addr = inet_addr(Configuracion::ip.c_str());
        this->stSocket.sin_family = AF_INET; // AF_INET -> Familia TCP/IP
        this->stSocket.sin_port = htons(Configuracion::puerto);

        // Conectamos con el servidor
        if (connect(this->fdSocket, (struct sockaddr *)&this->stSocket, sizeof(this->stSocket)) < 0) {

            for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
            
                Configuracion::vSolicitudes[i].setCodigo(3);
                Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (IP).");
            
            }

            if (Configuracion::debug != 0) {
        
                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (IP).\n");
            
            }
            
            conectado = false;

        } else {

            // Añadimos la estampa de tiempo inicial de la llamada
            for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
            
                Configuracion::vSolicitudes[i].setFechaInicioMinutaje(Fecha::fechaActual());
            
            }

            if (Configuracion::debug != 0) {
        
                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Conexion establecida.\n");
            
            }

        }

    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        this->dcb.DCBlength = sizeof(DCB);
        //  Obtenemos la configuracion actual
        if (GetCommState(this->hPuerto, &this->dcb) == false) {

            conectado = false;

        } else {

            // BaudRate
            // CBR_1200
            // CBR_2400
            // CBR_4800
            // CBR_9600
            // CBR_14400
            // CBR_19200

            // Paridad
            // EVENPARITY
            // MARKPARITY
            // NOPARITY
            // ODDPARITY
            // SPACEPARITY

            // Bit Parada  
            // ONESTOPBIT  1 stop bit.
            // ONE5STOPBITS    1.5 stop bits.
            // TWOSTOPBITS 2 stop bits. 

            //  9600 bps, 8 data bits, no parity, and 1 stop bit.
            this->dcb.BaudRate = CBR_9600;      //  baud rate
            this->dcb.ByteSize = 8;             //  data size, xmit and rcv
            this->dcb.Parity   = NOPARITY;      //  parity bit
            this->dcb.StopBits = ONESTOPBIT;    //  stop bit

            //  Establecemos la nueva configuracion 
            if (SetCommState(this->hPuerto, &this->dcb) == false) {
                
                conectado = false;

            }

            // Configuramos el timeout
            this->setTimeout(0);

        }
    
    }

    return conectado;

}

bool Comunicacion::llamar() {

    bool conectado = false;

    if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConectando con el modem llamante (GSM)...");

        enviar("AT");
        if (recibir("OK") == true) {

            if (Configuracion::debug != 0) {
                
                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nConectando con el modem remoto (GSM)...");
                enviar("ATDT" + Configuracion::telefono);
                if (recibir("CONNECT") == true) {

                    // enviar("AT&FS7=90S11=1S13=1S22=0S23=0S24=0S30=9600&D0Q0");
                    // Sleep(1000);

                    conectado = true;

                }
            
            }

        }

    } else {

        conectado = true;

    }

    return conectado;

}

void Comunicacion::colgar() {

    if (Configuracion::tipoComunicacion.compare("GSM") == 0) {  
        
        enviar("+++");
        Sleep(1000);
        enviar("ATH");

        // Añadimos la estampa de tiempo final de la llamada
        for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {

            Configuracion::vSolicitudes[i].setFechaFinMinutaje(Fecha::fechaActual());
        
        }

    }

}

// Metodo que enviara una trama al contador
void Comunicacion::enviar(vector<unsigned char> vTrama) {

    char trama[vTrama.size()];
    // Convertimos el vector en un char array para poder enviarlo
    copy(vTrama.begin(), vTrama.end(), trama);
    bool enviado = true;

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {

        int bytesEnviados = 0;
        // Enviamos la trama
        bytesEnviados = send(this->fdSocket, trama, sizeof(trama), 0);
        // cout << "[LOG] Se han enviado " << bytesEnviados << " bytes\n";
        if (bytesEnviados == -1) {

            enviado = false;
            
        }
    
    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        DWORD dwBytesWritten = 0;

        if (WriteFile(this->hPuerto, trama, sizeof(trama), &dwBytesWritten, NULL) != true) {

            enviado = false;

        }
    }

    if (enviado == false) {

        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[ERROR] No se ha podido enviar la trama.");

    } else if (Configuracion::debug == 2) {

        // Imprimimos la trama
        stringstream ss;
        ss  << "\n[ENVIADO] -> ";
        for (int i = 0; i < vTrama.size(); i++) {

            ss << "[" << i << "]";
            ss << hex << setw(2) << setfill('0') << (unsigned int)vTrama[i];
            ss << dec << "(" << (unsigned int)vTrama[i] << ") ";

        }
        //ss  << "\n";
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", ss.str());
    
    }
}

// Metodo que recibira una trama del contador
vector<unsigned char> Comunicacion::recibir() {

    vector<unsigned char> vTrama;
    int contador = 0;
    // Maximo de bytes que se puede recibir en una sola trama
    int maxContador = 1000;

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {

        char byte;        
        while (recv(this->fdSocket, &byte, 1, 0) == 1) {

            vTrama.push_back(byte);

            contador = contador + 1;

            if (contador >= maxContador) {

                vTrama.clear();
                break;

            }

        }


    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        unsigned char buffer;
        DWORD dwRead;
        bool leer = true;
        
        while (leer == true) {

            ReadFile(this->hPuerto, &buffer, 1, &dwRead, NULL);
            if (dwRead == 1) {
                
                vTrama.push_back(buffer);

                contador = contador + 1;

                if (contador >= maxContador) {

                    leer = false;
                    vTrama.clear();

                }

            } else {

                leer = false;

            }

        }

    }

    if (vTrama.empty() == true) {

        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[RECIBIDO] <- No se recibe nada.");

    } else if (Configuracion::debug == 2) {

        // Imprimimos la trama
        stringstream ss;
        ss  << "\n[RECIBIDO] [" + to_string(vTrama.size()) + "] <- ";
        for (int i = 0; i < vTrama.size(); i++) {

            ss << "[" << i << "]";
            ss << hex << setw(2) << setfill('0') << (unsigned int)vTrama[i];
            ss << dec << "(" << (unsigned int)vTrama[i] << ") ";

        }
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", ss.str());

    }

    return vTrama;

}

// Metodo que enviara un comando AT al modem
void Comunicacion::enviar(string comando) {

    DWORD dwBytesWritten = 0;
    comando += "\r";
    int n = comando.length();
    char buffer[comando.length() + 1];
    // Transformamos el string en un array de char
    strcpy(buffer, comando.c_str());

    // this->hPuerto    ->  Descriptor del puerto
    // buffer           ->  Buffer que contiene los datos que se enviaran
    // strlen(buffer)   ->  Numero de bytes que se escribiran
    // &dwBytesWritten  ->  Variable que recibe el numero de bytes escritos
    // NULL             ->  Si el parametro hPuerto no se ha abierto con FILE_FLAG_OVERLAPPED, es NULL
    if (WriteFile(this->hPuerto, buffer, strlen(buffer), &dwBytesWritten, NULL) != true) {

        if (Configuracion::debug != 0) {
        
                Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n[ERROR] No se ha podido enviar el comando AT.");
            
        }
        
    }

}

// Metodo que recibira la respuesta a un comando AT
bool Comunicacion::recibir(string respuestaAT) {

    bool conectado = false; 
    unsigned char buffer;
    vector<unsigned char> respuesta;
    // Errores que nos devuelve el modem
    vector<string> errores = {"CARRIER","DIALTONE","BUSY","ERROR"};
    DWORD dwRead;
    bool leer = true;
    // Numero intentos para leer el buffer del modem [30]
    int contador = 0;

    while (leer) {
        
        ReadFile(this->hPuerto, &buffer, 1, &dwRead, NULL);
        if (dwRead == 1) {
            
            respuesta.push_back(buffer);

        } else {
            
            string str(respuesta.begin(), respuesta.end());

            if(respuestaAT.compare("OK") == 0) {
                
                int found = str.find(respuestaAT);
                if (found != string::npos) {
                    
                    if (Configuracion::debug != 0) {
    
                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modem llamante OK");
                    
                    }
                    conectado = true;
                    leer = false;
                    break;

                }

            }
            
            if(respuestaAT.compare("CONNECT") == 0) {
                
                int found = str.find(respuestaAT);
                if (found != string::npos) {
                    
                    auto now = std::chrono::system_clock::now();
                    auto time = std::chrono::system_clock::to_time_t(now);
                    std::stringstream ss;
                    ss << put_time(std::localtime(&time), "%d/%m/%Y %H:%M:%S");

                    for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                
                        Configuracion::vSolicitudes[i].setFechaInicioMinutaje(ss.str());
                    
                    }

                    if (Configuracion::debug != 0) {
    
                        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " Modem remoto OK.\n");
                    
                    }
                    conectado = true;
                    leer = false;
                    break;

                }

            }

            contador = contador + 1;
            
            for (int i = 0; i < errores.size(); i++) {

                if(errores[i].compare("CARRIER") == 0) {
                    
                    int found = str.find(errores[i]);
                    if (found != string::npos) {

                        for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                    
                            Configuracion::vSolicitudes[i].setCodigo(2);
                            Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (GSM). NO CARRIER");
                        
                        }

                        if (Configuracion::debug != 0) {
    
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (GSM). NO CARRIER.\n");
                        
                        }
                        
                        leer = false;
                        break;

                    }

                } else if(errores[i].compare("DIALTONE") == 0) {
                    
                    int found = str.find(errores[i]);
                    if (found != string::npos) {

                        for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                    
                            Configuracion::vSolicitudes[i].setCodigo(2);
                            Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (GSM). NO DIALTONE.");
                        
                        }

                        if (Configuracion::debug != 0) {
    
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (GSM). NO DIALTONE.\n");
                        
                        }

                        leer = false;
                        break;

                    }

                } else if(errores[i].compare("BUSY") == 0) {
                    
                    int found = str.find(errores[i]);
                    if (found != string::npos) {

                        for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                    
                            Configuracion::vSolicitudes[i].setCodigo(2);
                            Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (GSM). BUSY.");
                        
                        }

                        if (Configuracion::debug != 0) {
    
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (GSM). BUSY.\n");
                        
                        }
                        
                        leer = false;
                        break;

                    }

                } else if(errores[i].compare("ERROR") == 0) {
                    
                    int found = str.find(errores[i]);
                    if (found != string::npos) {

                        for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {
                    
                            Configuracion::vSolicitudes[i].setCodigo(2);
                            Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (GSM). ERROR.");
                        
                        }

                        if (Configuracion::debug != 0) {
    
                            Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (GSM). ERROR.\n");
                        
                        }
                        
                        leer = false;
                        break;

                    }

                }

            }

            if (contador == 30) {

                for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {

                    Configuracion::vSolicitudes[i].setCodigo(2);
                    Configuracion::vSolicitudes[i].setMensaje("El modem remoto no responde (GSM). TIMEOUT.");
                
                }

                if (Configuracion::debug != 0) {
    
                    Archivo::escribir(Configuracion::nombreFicheroLog + ".log", " El modem remoto no responde (GSM). TIMEOUT.\n");
                    
                }
                
                leer = false;

            }

        }

    }
    
    return conectado;

}

// Metodo que cerrara el Socket/Puerto Serie
void Comunicacion::cerrar() {

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {

        // Cerramos el socket
        if(closesocket(this->fdSocket) == 0) {
            
            if(WSACleanup() == 0) {

                // Añadimos la estampa de tiempo final de la llamada
                for (int i = 0; i < Configuracion::vSolicitudes.size(); i++) {

                    if (Configuracion::vSolicitudes[i].getFechaFinMinutaje().compare("") != 0) {
            
                        Configuracion::vSolicitudes[i].setFechaFinMinutaje(Fecha::fechaActual());

                    }
                
                }

            }

        }

    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        CloseHandle(this->hPuerto);

    }

    if (Configuracion::debug != 0) {
        
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nFin de la comunicacion (" + Configuracion::tipoComunicacion + ").\n");
    
    }
}

void Comunicacion::setTimeout(int reintento) {

    int timeout = 500;

    if (Configuracion::tipoComunicacion.compare("GPRS") == 0) {

        timeout = 500 + (reintento * 300);

        // Aplicamos un timeout al socket
        setsockopt(this->fdSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(int));

        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTimeout establecido: " + to_string(timeout));

    } else if (Configuracion::tipoComunicacion.compare("GSM") == 0) {

        timeout = 800 + (reintento * 300);

        // Aplicamos un timeout al serial
        COMMTIMEOUTS timeouts = { 0 };
        GetCommTimeouts(this->hPuerto,&timeouts);
        timeouts.ReadIntervalTimeout = timeout;
        timeouts.ReadTotalTimeoutMultiplier = timeout;
        timeouts.ReadTotalTimeoutConstant = timeout;
        timeouts.WriteTotalTimeoutMultiplier = timeout;
        timeouts.WriteTotalTimeoutConstant = timeout;
        // Establecemos los timeout a la configuracion
        SetCommTimeouts(this->hPuerto, &timeouts);

        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nTimeout establecido: " + to_string(timeout));

    }

    if (Configuracion::debug == 2) {
        
        
    
    }

}
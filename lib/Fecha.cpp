#include "../include/Fecha.h"

bool Fecha::flagTM1;

bool Fecha::flagTM2;

// Constructor por defecto
Fecha::Fecha() {

    flagTM1 = false;
    flagTM2 = false;

}

// Metodo que convertira un string de fecha en hexadecimal
vector<unsigned char> Fecha::fechaStrToFechaHex(string fecha) {
    
    // Fecha -> dd/mm/aaaa hh:mm
    vector<unsigned char> vFecha;
    int hora = 0;
    int minuto = 1;
    vector<int> vDiaSemana = {5, 6, 0, 1, 2, 3, 4};

    struct tm *tmFecha;
    time_t curr_time;
    int diaBinario = 0;
    string binario = "";
    stringstream ss;
    unsigned n;
    int dia = 0;

    tmFecha = localtime(&curr_time);
    tmFecha->tm_hour = stoi(fecha.substr(11, 2));
    tmFecha->tm_min = stoi(fecha.substr(14, 2));
    tmFecha->tm_mday = stoi(fecha.substr(0, 2)) + 1; // Dia 
    tmFecha->tm_mon = stoi(fecha.substr(3, 2)) - 1; // Mes 
    tmFecha->tm_year = stoi(fecha.substr(6, 4)) - 1900; // Ano
    mktime(tmFecha);

    dia = tmFecha->tm_wday;
    dia = vDiaSemana[dia];

    string strBinario = "000";
    if (dia == 0) strBinario = "001";
    else if (dia == 1) strBinario = "010";
    else if (dia == 2) strBinario = "011";
    else if (dia == 3) strBinario = "100";
    else if (dia == 4) strBinario = "101";
    else if (dia == 5) strBinario = "110";
    else if (dia == 6) strBinario = "111";

    ss << hex << stoi(fecha.substr(0, 2));
    ss >> n;
    bitset<8> b(n);
    binario = b.to_string();
    
    binario[0] = strBinario[0];
    binario[1] = strBinario[1];
    binario[2] = strBinario[2];

    diaBinario = bitset<32>(binario).to_ulong();

    // dd/mm/aaaa hh:mm
    if (fecha.length() > 10) {
        hora = stoi(fecha.substr(11, 2));
        minuto = stoi(fecha.substr(14, 2));
    }
    vFecha.push_back(intToHex(minuto)); // Minuto  
    vFecha.push_back(intToHex(hora)); // Hora
    vFecha.push_back(intToHex(diaBinario)); // Dia 
    vFecha.push_back(intToHex(stoi(fecha.substr(3, 2)))); // Mes 
    vFecha.push_back(intToHex(stoi(fecha.substr(8, 2)))); // Ano

    return vFecha;

}

// Metodo que convertira una fecha en hexadecimal en string
vector<string> Fecha::fechaHexToFechaStr(vector<unsigned char> trama) {

    string fecha = "";
    bool correcto = true;
    vector<int> vHora;
    
    vector<string> vFecha;
    vFecha.push_back("");
    vFecha.push_back("");

    int dia = intToDia((int)trama[2]);
    
    //int mes = hexToInt((int)trama[3]);
    int mes = (int)trama[3];
    int ano = 2000 + (int)trama[4];
    vHora = intToHora((int)trama[1]);
    int hora = vHora[0];
    int bandera = vHora[1]; // {1: Horario de Verano | 0: Horario de Invierno}
    //int minuto = hexToInt((int)trama[0]);
    int minuto = (int)trama[0];
    
    // Si la trama no esta vacia
    if (trama.size() > 0) {

        if ( (dia < 1) | (dia > 31) ) {

            correcto = false;

        }

        if ( (mes < 1) || (mes > 12) ) {

            correcto = false;

        }

        if ( (ano <= 2016) || (mes > 3000) ) {

            correcto = false;

        }

        if ( (hora < 0) | (hora > 23) ) {

            correcto = false;

        }

        if ( (minuto < 0) | (hora > 59) ) {

            correcto = false;

        }

        // Si la comprobacion de la fecha ha sido correcta
        if (correcto == true) {

            // Dia
            if (dia < 10) {

                vFecha[0] += "0" + to_string(dia) + "/";

            } else {

                vFecha[0] += to_string(dia) + "/";

            }

            if (mes < 10) {

                vFecha[0] += "0" + to_string(mes) + "/"; // Mes
                
            } else {

                vFecha[0] += to_string(mes) + "/"; // Mes

            }

            vFecha[0] += to_string(ano) + " "; // Ano
            
            // Hora
            if (hora < 10) {

                vFecha[0] += "0" + to_string(hora) + ":";

            } else {

                vFecha[0] += to_string(hora) + ":";

            }
            
            // Minuto
            if (minuto < 10) {

                vFecha[0] += "0" + to_string(minuto);

            } else {

                vFecha[0] += to_string(minuto);

            }

            // Bandera 
            if (bandera == 1) {

                vFecha[1] = "V";

            } else {

                vFecha[1] = "I";

            }
        
        }

    } else {

        correcto = false;

    }

    if (correcto == false) {

        vFecha[0] = "";
        vFecha[1] = "";

    }
    
    return vFecha;

}

// Metodo que nos dira la fecha de la siguiente curva a descargar
string Fecha::siguienteFecha(vector<unsigned char> vFecha, string tipoCurva) {
    
    string strFecha = "";
    char fecha[20];
    char hora[20];
    int horas = 0;
    int minutos = 0;
    int sumar = 60;
    vector<string> vAuxFecha = fechaHexToFechaStr(vFecha);

    time_t curr_time;
    struct tm *tmFecha;
    time(&curr_time);
	tmFecha = localtime(&curr_time);

    if (vAuxFecha[0].compare("") != 0) {

        // Convertirmos la fecha a struct tm
        // dd/mm/aaaa hh:mm
        tmFecha->tm_hour = stoi(vAuxFecha[0].substr(11, 2));
        tmFecha->tm_min = stoi(vAuxFecha[0].substr(14, 2));
        tmFecha->tm_mday = stoi(vAuxFecha[0].substr(0, 2)); // Dia 
        tmFecha->tm_mon = stoi(vAuxFecha[0].substr(3, 2)) - 1; // Mes 
        tmFecha->tm_year = stoi(vAuxFecha[0].substr(6, 4)) - 1900; // Ano
        //tmFecha->tm_isdst = 1;

        if ( (tmFecha->tm_mon == 9) && (tmFecha->tm_mday == 28) && (tmFecha->tm_hour == 2) ) {

            if ( (tipoCurva.compare("TM1") == 0) && (flagTM1 == false) && (tmFecha->tm_min == 0) ) {
            
                tmFecha->tm_isdst = 1;
                flagTM1 = true;
            
            } else if ( (tipoCurva.compare("TM2") == 0) && (flagTM2 == false) && (tmFecha->tm_min == 45) ) {
            
                tmFecha->tm_isdst = 1;
                flagTM2 = true;
            
            } else {

                tmFecha->tm_isdst = 0;

            }

        } else {

            tmFecha->tm_isdst = 0;

        }

        mktime(tmFecha);

        if (tipoCurva.compare("TM2") == 0) {
            
            sumar = 15;

        }

        tmFecha->tm_min += sumar;

        mktime(tmFecha);

        string strAuxFecha = "";
    
        strftime(fecha, sizeof(fecha), "%d/%m/%Y", tmFecha);
        strftime(hora, sizeof(hora), "%H:%M", tmFecha);
    
        string strDate(fecha);
        string strHora(hora);
        strFecha = strDate + " " + strHora;
    
    }    

    return strFecha;

}

// Metodo que nos dira la fecha de la siguiente curva a descargar
string Fecha::siguienteFecha(string strFecha, string tipoCurva) {
    
    char fecha[20];
    char hora[20];
    int horas = 0;
    int minutos = 0;
    int sumar = 60;

    time_t curr_time;
    struct tm *tmFecha;
    time(&curr_time);
	tmFecha = localtime(&curr_time);

    if (strFecha.compare("") != 0) {

        // Convertirmos la fecha a struct tm
        // dd/mm/aaaa hh:mm
        tmFecha->tm_hour = stoi(strFecha.substr(11, 2));
        tmFecha->tm_min = stoi(strFecha.substr(14, 2));
        tmFecha->tm_mday = stoi(strFecha.substr(0, 2)); // Dia 
        tmFecha->tm_mon = stoi(strFecha.substr(3, 2)) - 1; // Mes 
        tmFecha->tm_year = stoi(strFecha.substr(6, 4)) - 1900; // Ano
        //tmFecha->tm_isdst = 1;

        if ( (tmFecha->tm_mon == 9) && (tmFecha->tm_mday == 28) && (tmFecha->tm_hour == 2) ) {

            tmFecha->tm_isdst = 1;

        } else {

            tmFecha->tm_isdst = 0;

        }

        mktime(tmFecha);

        if (tipoCurva.compare("TM2") == 0) {
            
            sumar = 15;

        }

        tmFecha->tm_min += sumar;

        mktime(tmFecha);

        string strAuxFecha = "";
    
        strftime(fecha, sizeof(fecha), "%d/%m/%Y", tmFecha);
        strftime(hora, sizeof(hora), "%H:%M", tmFecha);
    
        string strDate(fecha);
        string strHora(hora);
        strFecha = strDate + " " + strHora;
    
    }    

    return strFecha;

}

// Metodo que nos dira cual debe de ser la fecha de la primera curva a descargar
string Fecha::primerRegistro(string strFecha) {

    char fecha[20];
    char hora[20];
    time_t curr_time;
    struct tm *tmFecha;
    time(&curr_time);
	tmFecha = localtime(&curr_time);
    string strResult = "";
    int sumar = 0;

    if (strFecha.compare("") != 0) {

        // Convertirmos la fecha a struct tm
        // dd/mm/aaaa hh:mm
        tmFecha->tm_hour = stoi(strFecha.substr(11, 2)); // Hora
        int minuto = stoi(strFecha.substr(14, 2));
        tmFecha->tm_min = minuto; // Minuto
        tmFecha->tm_mday = stoi(strFecha.substr(0, 2)); // Dia 
        tmFecha->tm_mon = stoi(strFecha.substr(3, 2)) - 1; // Mes 
        tmFecha->tm_year = stoi(strFecha.substr(6, 4)) - 1900; // Ano

        mktime(tmFecha);

        if ( (minuto == 0) || (minuto == 15) || (minuto == 30) || (minuto == 45) ) {

            sumar = 0;

        } else {
        
            if ( (minuto > 0) && (minuto < 15) ) {

                sumar = 15 - minuto;

            } else if ( (minuto > 15) && (minuto < 30) ) {

                sumar = 30 - minuto;

            } if ( (minuto > 30) && (minuto < 45) ) {

                sumar = 45 - minuto;

            } if ( (minuto > 45) && (minuto < 60) ) {

                sumar = 60 - minuto;

            } 
        
        }
        tmFecha->tm_min += sumar;

        mktime(tmFecha);

        string strAuxFecha = "";

        strftime(fecha, sizeof(fecha), "%d/%m/%Y", tmFecha);
        strftime(hora, sizeof(hora), "%H:%M", tmFecha);

        string strDate(fecha);
        string strHora(hora);
        strResult = strDate + " " + strHora;

    }

    return strResult;

}

// Metodo que nos dira si la fecha es valida
bool Fecha::validaFecha(string strFecha) {
    
    bool fechaValida = true;
    int dia = 0;
    int mes = 0;
    int ano = 0;
    int hora = 0;
    int minuto = 0;

    if (strFecha.compare("") == 0) {

        fechaValida = false;

    } else {

        // Comprobamos el formato de la fecha --> [dd/mm/aaaa hh:mm]
        if (strFecha.size() == 16) {

            // Dia
            dia = stringToInt(strFecha.substr(0, 2));
            if (dia == -1) {

                fechaValida = false;

            } else {

                if ( (dia < 1) || (dia > 31) ) {

                    fechaValida = false;

                }

            }

            // Mes
            mes = stringToInt(strFecha.substr(3, 2));
            if (mes == -1) {

                fechaValida =  false;
                
            } else {

                if ( (mes < 1) || (mes > 12) ) {

                    fechaValida =  false;
                    
                }

            }

            // Ano
            ano = stringToInt(strFecha.substr(6, 4));
            if (ano == -1) {

                fechaValida =  false;
                
            } else {

                if ( (ano < 2010) || (ano > 3000) ) {

                    fechaValida =  false;

                }

            }

            // Hora
            hora = stringToInt(strFecha.substr(11, 2));
            if (hora == -1) {

                fechaValida =  false;
                
            } else {

                if ( (hora < 0) || (hora > 23) ) {

                    fechaValida =  false;
                    
                }

            }

            // Minutos
            minuto = stringToInt(strFecha.substr(14, 2));
            if (minuto == -1) {

                fechaValida =  false;
                
            } else {

                if ( (minuto < 0) || (minuto > 59) ) {

                    fechaValida =  false;
                    
                }

            }

        } else {

            fechaValida =  false;

        }
    }

    return fechaValida;

}

// Metodo que nos dira si la fecha incial es <= que la fecha final
int Fecha::comparaFechas(string strFechaIni, string strFechaFin) {
    
    // Codigo:
    //    -1: strFechaIni > strFechaFin
    //     1: strFechaIni <= strFechaFin
    int resultado = 1;

    // Fecha Inicio
    int diaIni = stringToInt(strFechaIni.substr(0, 2));
    int mesIni = stringToInt(strFechaIni.substr(3, 2));
    int anoIni = stringToInt(strFechaIni.substr(6, 4));
    int horaIni = stringToInt(strFechaIni.substr(11, 2));
    int minutoIni = stringToInt(strFechaIni.substr(14, 2));

    // Fecha Fin
    int diaFin = stringToInt(strFechaFin.substr(0, 2));
    int mesFin = stringToInt(strFechaFin.substr(3, 2));
    int anoFin = stringToInt(strFechaFin.substr(6, 4));
    int horaFin = stringToInt(strFechaFin.substr(11, 2));
    int minutoFin = stringToInt(strFechaFin.substr(14, 2));

    if (strFechaIni.compare("") == 0) {

        resultado = -1;

    } else {

        // 17/09/2019 10:30 > 17/09/2018 10:30
        if (anoIni > anoFin) {

            resultado = -1;

        } else {
            
            // 17/10/2018 10:30 > 17/09/2018 10:30
            if (mesIni > mesFin) {

                // 17/10/2019 10:30 >= 17/09/2018 10:30
                if (anoIni >= anoFin) {

                    resultado = -1;

                }

            } else {

                // 17/09/2018 10:30 == 17/09/2018 10:30
                if (mesIni == mesFin) {

                    // 20/09/2018 10:30 > 17/09/2018 10:30
                    if (diaIni > diaFin) {

                        resultado = -1;

                    } else if (diaIni == diaFin) { // 17/09/2018 10:30 == 17/09/2018 10:30

                        // 17/09/2018 12:30 > 17/09/2018 10:30
                        if (horaIni > horaFin) {

                            resultado = -1;

                        } else if (horaIni == horaFin) { // 17/09/2018 10:30 == 17/09/2018 10:30

                            // 17/09/2018 10:50 > 17/09/2018 10:30
                            if (minutoIni > minutoFin) {

                                resultado = -1;

                            }

                        }

                    }

                }

            }
        }

    }

    return resultado;

}

// Metodo que convertira un entero en dia
int Fecha::intToDia(int number) {

    int dia = 0;
    string binario = "";
    stringstream ss;
    unsigned n;

    ss << hex << number;
    ss >> n;
    bitset<8> b(n);
    binario = b.to_string();
    
    for (int i = 0; i < 3; i++) {

        binario[i] = '0';

    }
    dia = bitset<32>(binario).to_ulong();

    return dia;

}

// Metodo que convertira un entero en hora con su bandera
vector<int> Fecha::intToHora(int number) {

    vector<int> vDia;
    string binario = "";
    stringstream ss;

    vDia.push_back(0); // Dia del mes
    vDia.push_back(0); // Bandera

    ss << hex << number;
    unsigned n;
    ss >> n;
    bitset<8> b(n);
    binario = b.to_string();

    // Comprobamos si es horario de verano
    if (binario[0] == '1') {

        vDia[1] = 1;

    }

    for (int i = 0; i < 3; i++) {

        binario[i] = '0';

    }
    vDia[0] = bitset<32>(binario).to_ulong();

    return vDia;

}

// Metodo para convertir un entero a hexadecimal
unsigned char Fecha::intToHex(int numero) {

    stringstream stream;

    stream << hex << numero;
    // Convertimos a unsigned char el valor
    union U
    {
        unsigned int valor;
        unsigned char componente;
    };
    U u;
    stringstream SS(stream.str());
    SS >> hex >> u.valor;
    
    return u.componente;

}

// Metodo que convertira un string a entero
int Fecha::stringToInt(string str) {
    
    int numero = 0;

    try {   

        numero = stoi(str);

    } catch (invalid_argument& e){

        numero = -1;

    } catch (out_of_range& e) {

        numero = -1;

    }

    return numero;
}

// Metodo que nos devolvera la fecha actual
string Fecha::fechaActual() {
    
    stringstream ss;
    auto now = chrono::system_clock::now();
    auto time = chrono::system_clock::to_time_t(now);
    
    ss << put_time(localtime(&time), "%d/%m/%Y %H:%M:%S");

    return ss.str();

}

// Metodo que nos devolvera la estampa de tiempo de la fecha actual
string Fecha::estampaFechaActual() {
    
    time_t timestamp_sec;
    time(&timestamp_sec);
    stringstream date;
    date << timestamp_sec;

    return date.str();

}

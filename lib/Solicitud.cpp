#include "../include/Solicitud.h"

// Constructor por defecto
Solicitud::Solicitud() { 

    setDefault();

}

// Metodo que añadira una fecha al vector de tuplas de fechas de curvas descargadas
void Solicitud::addFechaCurva(string strFecha, string strBandera) {

    this->vFechaCurvasDescargadas.push_back(tuple<string, string>(strFecha, strBandera));

}

// Metodo que añadira una fecha al vector de tuplas de fechas de cierres descargados
void Solicitud::addFechaCierre(string strFechaInicio, string strFechaFin, int contrato, int tarifa) {

    this->vFechaCierresDescargados.push_back(tuple<string, string, int, int>(strFechaInicio, strFechaFin, contrato, tarifa));

}

// Metodo que comprobara si existe una fecha en el vector de tuplas de fechas de curvas descargadas
bool Solicitud::existeFechaCurva(string strFecha, string strBandera) {

    bool encontrado = false;
    auto fechaRegistro = make_tuple(strFecha, strBandera);
    
    for (int i = 0; i < this->vFechaCurvasDescargadas.size() && encontrado == false; i++) {

        if (strFecha.compare(get<0>(this->vFechaCurvasDescargadas[i])) == 0) {

            if (strBandera.compare(get<1>(this->vFechaCurvasDescargadas[i])) == 0) {

                encontrado = true;

            }

        }

    }

    // Buscamos la fecha de la curva en el vector de fechas
    // if (find(this->vFechaCurvasDescargadas.begin(), this->vFechaCurvasDescargadas.end(), fechaRegistro) != this->vFechaCurvasDescargadas.end()) {

    //     encontrado = true;

    // }

    return encontrado;

}

// Metodo que comprobara si existe una fecha en el vector de tuplas de fechas de cierres descargados
bool Solicitud::existeFechaCierre(string strFechaInicio, string strFechaFin, int contrato, int tarifa) {

    bool encontrado = false;
    auto fechaRegistro = make_tuple(strFechaInicio, strFechaFin, contrato, tarifa);

    // Buscamos la fecha de la curva en el vector de fechas
    if (find(this->vFechaCierresDescargados.begin(), this->vFechaCierresDescargados.end(), fechaRegistro) != this->vFechaCierresDescargados.end()) {

        encontrado = true;

    }

    return encontrado;

}

// Metodo que devolvera a la solicitud a su estado por defecto
void Solicitud::setDefault() {

    // Valores por defecto del resultado de la solicitud
    this->codigo = 0;
    this->mensaje = "Sin Errores, Todo Ok.";
    this->fechaInicioMinutaje = "";
    this->fechaFinMinutaje = "";
    this->registrosDescargados = 0;

    // Valores por defecto de la informacion de la solicitud
    this->tipoCurva = "";
    this->modoLectura = "";
    this->fechaInicio = "";
    this->fechaFin = "";
    this->nombreFicheroCSV = "";
    this->referencia = -1;

    this->vFechaCurvasDescargadas.clear();
    this->vFechaCierresDescargados.clear();

}

void Solicitud::aumentaRegistrosDescargados() {

    this->registrosDescargados = this->registrosDescargados + 1;

}

void Solicitud::limpiaVectorFechaCurvas() {

    this->vFechaCurvasDescargadas.clear();

}

// SETTERS
void Solicitud::setCodigo(int codigo) {

    this->codigo = codigo;

}

void Solicitud::setMensaje(string mensaje) {

    this->mensaje = mensaje;

}

void Solicitud::setFechaInicioMinutaje(string fechaInicioMinutaje) {

    this->fechaInicioMinutaje = fechaInicioMinutaje;

}

void Solicitud::setFechaFinMinutaje(string fechaFinMinutaje) {

    this->fechaFinMinutaje = fechaFinMinutaje;

}

void Solicitud::setRegistrosDescargados(int registrosDescargados) {

    this->registrosDescargados = registrosDescargados;

}

void Solicitud::setTipoCurva(string tipoCurva) {

    this->tipoCurva = tipoCurva;

}

void Solicitud::setModoLectura(string modoLectura) {

    this->modoLectura = modoLectura;

}

void Solicitud::setFechaInicio(string fechaInicio) {

    this->fechaInicio = fechaInicio;

}

void Solicitud::setFechaFin(string fechaFin) {

    this->fechaFin = fechaFin;

}

void Solicitud::setNombreFicheroCSV(string nombreFicheroCSV) {

    this->nombreFicheroCSV = nombreFicheroCSV;

}

void Solicitud::setReferencia(int referencia) {

    this->referencia = referencia;

}

void Solicitud::setFechasCurvasDescargadas(vector<tuple<string, string>> vFechas) {

    this->vFechaCurvasDescargadas = vFechas;

} 

void Solicitud::setFechasCierresDescargados(vector<tuple<string, string, int, int>> vFechas) {

    this->vFechaCierresDescargados = vFechas;

}


// GETTERS
int Solicitud::getCodigo() {

    return this->codigo;

}

string Solicitud::getMensaje() {

    return this->mensaje;

}

string Solicitud::getFechaInicioMinutaje() {

    return this->fechaInicioMinutaje;

}

string Solicitud::getFechaFinMinutaje() {

    return this->fechaFinMinutaje;

}

int Solicitud::getRegistrosDescargados() {

    return this->registrosDescargados;

}

string Solicitud::getTipoCurva() {

    return this->tipoCurva;

}

string Solicitud::getModoLectura() {

    return this->modoLectura;

}

string Solicitud::getFechaInicio() {

    return this->fechaInicio;

}

string Solicitud::getFechaFin() {

    return this->fechaFin;

}

string Solicitud::getNombreFicheroCSV() {

    return this->nombreFicheroCSV;

}

int Solicitud::getReferencia() {

    return this->referencia;

}

vector<string> Solicitud::getUltimaCurvaDescargada() {

    vector<string> vUltimaCurva;

    if (this->vFechaCurvasDescargadas.empty() == false) {

        vUltimaCurva.push_back(get<0>(this->vFechaCurvasDescargadas[this->vFechaCurvasDescargadas.size() - 1]));
        vUltimaCurva.push_back(get<1>(this->vFechaCurvasDescargadas[this->vFechaCurvasDescargadas.size() - 1]));

    } else {

        vUltimaCurva.push_back("");
        vUltimaCurva.push_back("");

    }

    return vUltimaCurva;

}

void Solicitud::setFechaSiguienteCurva(string fecha, string bandera) {

    this->vSiguienteCurva.clear();

    this->vSiguienteCurva.push_back(fecha);
    this->vSiguienteCurva.push_back(bandera);  

}

vector<string> Solicitud::getFechaSiguienteCurva() {

    if (this->vSiguienteCurva.empty() == true) {

        vSiguienteCurva.push_back("");
        vSiguienteCurva.push_back("");

    }

    return this->vSiguienteCurva;

}

vector<tuple<string, string>> Solicitud::getFechasCurvasDescargadas() {

    return this->vFechaCurvasDescargadas;

}

vector<tuple<string, string, int, int>> Solicitud::getFechasCierresDescargados() {

    return this->vFechaCierresDescargados;
}


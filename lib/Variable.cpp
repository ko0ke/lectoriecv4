#include "../include/Variable.h"

// FLAG DE TRAMA VARIABLE
const unsigned char Variable::FLAG_TRAMA_VARIABLE   = 0x68;
const unsigned char Variable::FLAG_FIN              = 0x16;

// CAMPOS DE CONTROL DEL MAESTRO
const unsigned char Variable::INICIO_SESION     = 0x73;
const unsigned char Variable::FIN_SESION        = 0x53;

// IDENTIFICADOR DE TIPO
const unsigned char Variable::INICIO_SESION_ENVIO_CLAVE     = 0xB7;
const unsigned char Variable::LECTURA_BLOQUE_ABSOLUTA       = 0xBD;
const unsigned char Variable::LECTURA_BLOQUE_INCREMENTAL    = 0xBE;
const unsigned char Variable::LECTURA_FECHA_HORA            = 0x67;
const unsigned char Variable::CIERRE                        = 0x86;
const unsigned char Variable::FINALIZAR_SESION              = 0xBB;
const unsigned char Variable::LEER_CONFIGURACION            = 0x8D;

// CAUSA
const unsigned char Variable::ACTIVACION        = 0x06;
const unsigned char Variable::PETICION          = 0x05;

// DIRECCIONES DE REGISTRO
const unsigned char Variable::HORARIA           = 0x0B;
const unsigned char Variable::CUARTAHORARIA     = 0x0C;
const unsigned char Variable::CIERRE_CONTRATO_1 = 0x86;
const unsigned char Variable::CIERRE_CONTRATO_2 = 0x87;
const unsigned char Variable::CIERRE_CONTRATO_3 = 0x88;

// DIRECCIONES DE OBJETO
const unsigned char Variable::TOTALES_INTEGRADOS_CON_RESERVAS       = 0x09;
const unsigned char Variable::TOTALES_INTEGRADOS_SIN_RESERVAS       = 0x0A;
const unsigned char Variable::TOTALES_INTEGRADOS_PURO_SIN_RESERVAS  = 0x0B;

Variable::Variable() {
    
}

// Inicio sesion y envio clave acceso
vector<unsigned char> Variable::C_AC_NA_2() {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(INICIO_SESION_ENVIO_CLAVE);
    // Numero de objetos
    trama.push_back(0x01); // SQ = 0, N = 1. Cualificador estructura variable. [N = 1] -> 1 objeto de informacion 
    trama.push_back(ACTIVACION);
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    trama.push_back(0x00);
    // Objetos
    // Clave
    trama.push_back(Configuracion::clave & 0xFF);
    trama.push_back((Configuracion::clave >> 8) & 0xFF);
    trama.push_back((Configuracion::clave >> 16) & 0xFF);
    trama.push_back((Configuracion::clave >> 24) & 0xFF);

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo + 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud;

    if (Configuracion::debug == 2) {
    
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n#################################################");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n## Inicio de Sesion y Envio de Clave de Acceso ##");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n#################################################");
    
    }

    return trama;
}

// Registros absolutos
vector<unsigned char> Variable::C_CB_NT_2(int solicitud) {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(LECTURA_BLOQUE_ABSOLUTA);
    // Numero de objetos
    trama.push_back(0x01); // SQ = 0, N = 1. Cualificador estructura variable. [N = 1] -> 1 objeto de informacion
    // Causa
    trama.push_back(ACTIVACION); 

    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    // Direccion de registro
    if (Configuracion::vSolicitudes[solicitud].getTipoCurva().compare("TM1") == 0) {
        trama.push_back(HORARIA);
    } else if (Configuracion::vSolicitudes[solicitud].getTipoCurva().compare("TM2") == 0) {
        trama.push_back(CUARTAHORARIA);
    }

    // Objetos
    // Direccion de objeto
    trama.push_back(TOTALES_INTEGRADOS_CON_RESERVAS);
    // Etiquetas de tiempo
    // Fecha Inicio
    vector<unsigned char> vFechaInicio = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaInicio());
    trama.push_back(vFechaInicio[0]); // Minuto
    trama.push_back(vFechaInicio[1]); // Hora
    trama.push_back(vFechaInicio[2]); // Dia
    trama.push_back(vFechaInicio[3]); // Mes
    trama.push_back(vFechaInicio[4]); // Año
    // Fecha Fin
    vector<unsigned char> vFechaFin = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaFin());
    trama.push_back(vFechaFin[0]); // Minuto
    trama.push_back(vFechaFin[1]); // Hora
    trama.push_back(vFechaFin[2]); // Dia
    trama.push_back(vFechaFin[3]); // Mes
    trama.push_back(vFechaFin[4]); // Año
    
    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo + 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud;

    return trama;
}

// Registros incrementales
vector<unsigned char> Variable::C_CB_NU_2(int solicitud) {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // 
    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(LECTURA_BLOQUE_INCREMENTAL);
    // Numero de objetos
    trama.push_back(0x01); // SQ = 0, N = 1. Cualificador estructura variable. [N = 1] -> 1 objeto de informacion
    // Causa
    trama.push_back(ACTIVACION); 
    
    // 
    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    // Direccion de registro
    if (Configuracion::vSolicitudes[solicitud].getTipoCurva().compare("TM1") == 0) {
        trama.push_back(HORARIA);
    } else if (Configuracion::vSolicitudes[solicitud].getTipoCurva().compare("TM2") == 0) {
        trama.push_back(CUARTAHORARIA);
    }

    // 
    // Objetos
    // Direccion de objeto
    trama.push_back(TOTALES_INTEGRADOS_CON_RESERVAS);
    //trama.push_back(TOTALES_INTEGRADOS_PURO_SIN_RESERVAS);
    //trama.push_back(TOTALES_INTEGRADOS_SIN_RESERVAS);
    // Etiquetas de tiempo
    // Fecha Inicio
    vector<unsigned char> vFechaInicio = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaInicio());
    trama.push_back(vFechaInicio[0]); // Minuto
    trama.push_back(vFechaInicio[1]); // Hora
    trama.push_back(vFechaInicio[2]); // Dia
    //trama.push_back(0xA1); // Dia
    trama.push_back(vFechaInicio[3]); // Mes
    trama.push_back(vFechaInicio[4]); // Año

    // Fecha Fin
    vector<unsigned char> vFechaFin = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaFin());
    trama.push_back(vFechaFin[0]); // Minuto
    trama.push_back(vFechaFin[1]); // Hora
    trama.push_back(vFechaFin[2]); // Dia
    //trama.push_back(0xA1); // Dia
    trama.push_back(vFechaFin[3]); // Mes
    trama.push_back(vFechaFin[4]); // Año
    
    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo y 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud;

    return trama;
}

// Cierre
vector<unsigned char> Variable::C_TA_VM_2(int solicitud, int contrato) {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // 
    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(CIERRE);
    // Numero de objetos
    trama.push_back(0x01); // SQ = 0, N = 1. Cualificador estructura variable. [N = 1] -> 1 objeto de informacion
    // Causa
    trama.push_back(ACTIVACION); 
    
    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    // Direccion de registro
    switch (contrato)
    {
        case 1: // Contrato 1
            trama.push_back(CIERRE_CONTRATO_1);
            break;
        case 2: // Contrato 2
            trama.push_back(CIERRE_CONTRATO_2);
            break;
        case 3: // Contrato 3
            trama.push_back(CIERRE_CONTRATO_3);
            break;
    
        default:
            break;
    }
    
    // 
    // Objetos
    // Etiquetas de tiempo
    // Fecha Inicio
    vector<unsigned char> vFechaInicio = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaInicio());
    trama.push_back(vFechaInicio[0]); // Minuto
    trama.push_back(vFechaInicio[1]); // Hora
    trama.push_back(vFechaInicio[2]); // Dia
    trama.push_back(vFechaInicio[3]); // Mes
    trama.push_back(vFechaInicio[4]); // Año
    // Fecha Fin
    vector<unsigned char> vFechaFin = Fecha::fechaStrToFechaHex(Configuracion::vSolicitudes[solicitud].getFechaFin());
    trama.push_back(vFechaFin[0]); // Minuto
    trama.push_back(vFechaFin[1]); // Hora
    trama.push_back(vFechaFin[2]); // Dia
    trama.push_back(vFechaFin[3]); // Mes
    trama.push_back(vFechaFin[4]); // Año

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo y 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud;

    return trama;
}

// Finalizar sesion
vector<unsigned char> Variable::C_FS_NA_2() {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(FIN_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(FINALIZAR_SESION);
    // Numero de objetos
    trama.push_back(0x00);
    // Causa
    trama.push_back(ACTIVACION);

    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    // Direccion de registro
    trama.push_back(0x00);    

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo y 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud; 

    if (Configuracion::debug == 2) {
    
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n######################");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n## Finalizar Sesion ##");
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\n######################");
    
    }

    return trama;
}

// Leer la configuracion del equipo RM
vector<unsigned char> Variable::C_RM_NA_2() {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(LEER_CONFIGURACION);
    // Numero de objetos
    trama.push_back(0x00);
    // Causa
    trama.push_back(PETICION);

    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);

    // Direccion de registro
    trama.push_back(0x00);
    trama.push_back(0x00);   

    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo y 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud; 

    if (Configuracion::debug == 2) {
    
        Archivo::escribir(Configuracion::nombreFicheroLog + ".log", "\nNumero de Serie");
    
    }

    return trama;

}

// Leer la fecha y la hora del contador
vector<unsigned char> Variable::C_TI_NA_2() {
    vector<unsigned char> trama;

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Longitud de la trama
    trama.push_back(0x00);
    trama.push_back(0x00);

    // Flag de Inicio
    trama.push_back(this->FLAG_TRAMA_VARIABLE);

    // Campo de Control C
    trama.push_back(INICIO_SESION);

    // Direccion de enlace
    trama.push_back(Configuracion::dirEnlace & 0xFF);
    trama.push_back((Configuracion::dirEnlace >> 8) & 0xFF);

    // ASDU
    // Identificador unidad de datos
    // Identificador del tipo
    trama.push_back(LECTURA_FECHA_HORA);
    // Numero de objetos
    trama.push_back(0x00); // SQ = 0, N = 1. Cualificador estructura variable. [N = 1] -> 1 objeto de informacion
    // Causa
    trama.push_back(PETICION);

    // Direccion comun ASDU
    // Punto de Medida
    trama.push_back(Configuracion::puntoMedida & 0xFF);
    trama.push_back((Configuracion::puntoMedida >> 8) & 0xFF);
    
    trama.push_back(0x00);
    
    // Checksum
    trama.push_back(getChecksum(trama));

    // Flag de Fin
    trama.push_back(this->FLAG_FIN);

    // Actualizamos los bytes en los que decimos la longitud de la trama
    int longitud = (trama.size() - 6); // 6 = 4 bytes del comienzo + 2 bytes del final
    trama[1] = longitud;
    trama[2] = longitud;

    return trama;
}

// Calculo del checksum de la trama
unsigned char Variable::getChecksum(vector<unsigned char> trama) {
    unsigned char cheksum = 0x00;
    // Si la trama es de tipo variable el calculo del checksum empieza en la posicion 5
    int posicion = 5;
    // Byte con la suma de todos los bytes comenzando por el campo de control (incluido) hasta el cheksum (no incluido)
    int suma = 0;
    for (int i = (posicion - 1); i < trama.size(); i++) {
        // Sumamos todos los bytes desde la posicion hasta el final
        suma = suma + (int)trama[i];
    }
    cheksum = intToHex(suma);

    return cheksum;
}

// Metodo para convertir un entero a hexadecimal
unsigned char Variable::intToHex(int numero) {
    stringstream stream;

    stream << hex << numero;
    // Convertimos a unsigned char el valor
    union U
    {
        unsigned int valor;
        unsigned char componente;
    };
    U u;

    stringstream SS(stream.str());
    SS >> hex >> u.valor;
    
    return u.componente;
}
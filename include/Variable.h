#ifndef _variable_h_
#define _variable_h_

#include "Archivo.h"
#include "Configuracion.h"

#include <vector>

using namespace std;

class Variable
{
    private:

        // CAMPOS DE CONTROL DEL MAESTRO
        static const unsigned char INICIO_SESION; 
        static const unsigned char FIN_SESION;

        // IDENTIFICADOR DE TIPO
        static const unsigned char INICIO_SESION_ENVIO_CLAVE;
        static const unsigned char LECTURA_BLOQUE_ABSOLUTA;
        static const unsigned char LECTURA_BLOQUE_INCREMENTAL;
        static const unsigned char LECTURA_FECHA_HORA;
        static const unsigned char CIERRE;
        static const unsigned char FINALIZAR_SESION;
        static const unsigned char LEER_CONFIGURACION;

        // CAUSA
        static const unsigned char ACTIVACION;
        static const unsigned char PETICION;

        // DIRECCIONES DE REGISTRO
        static const unsigned char HORARIA;
        static const unsigned char CUARTAHORARIA;
        static const unsigned char CIERRE_CONTRATO_1;
        static const unsigned char CIERRE_CONTRATO_2;
        static const unsigned char CIERRE_CONTRATO_3;

        // DIRECCIONES DE OBJETO
        static const unsigned char TOTALES_INTEGRADOS_CON_RESERVAS;
        static const unsigned char TOTALES_INTEGRADOS_SIN_RESERVAS;
        static const unsigned char TOTALES_INTEGRADOS_PURO_SIN_RESERVAS;

        // Metodo para convertir un entero a hexadecimal
        unsigned char intToHex(int);

        // Calculo del checksum de la trama
        unsigned char getChecksum(vector<unsigned char>);

    public:

        // FLAG DE TRAMA VARIABLE
        static const unsigned char FLAG_TRAMA_VARIABLE;

        // FLAG DE FIN DE TRAMA
        static const unsigned char FLAG_FIN;

        // Constructor por defecto
        Variable();

        // Inicio sesion y envio clave acceso
        vector<unsigned char> C_AC_NA_2();
        // Registros absolutos
        vector<unsigned char> C_CB_NT_2(int);
        // Registros incrementales
        vector<unsigned char> C_CB_NU_2(int);
        // Cierre
        vector<unsigned char> C_TA_VM_2(int, int);
        // Finalizar sesion
        vector<unsigned char> C_FS_NA_2();
        // Leer configuracion del contador
        vector<unsigned char> C_RM_NA_2();
        // Leer fecha y hora del contador
        vector<unsigned char> C_TI_NA_2();

};

#endif
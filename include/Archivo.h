#ifndef _archivo_h_
#define _archivo_h_

#include <string>
#include <fstream>

using namespace std;

class Archivo
{
    private:

        // Cabeceras que tendra el CSV de CIERREs
        static const string CABECERA_CURVA_CSV;

        // Cabeceras que tendra el CSV de CURVAs
        static const string CABECERA_CIERRE_CSV;

        // Metodo que comprobara si el fichero ya existe
        static bool existeFichero(string);

    public:

        // Constructor por defecto
        Archivo();

        // Metodo que escribira una linea en el fichero pasado por parametro
        static void escribir(string, string);

};

#endif
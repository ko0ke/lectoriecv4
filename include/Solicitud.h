#ifndef _solicitud_h_
#define _solicitud_h_

#include "Archivo.h"

#include <string>
#include <vector>
#include <tuple>
#include <algorithm>

using namespace std;

class Solicitud
{
    private:

        /* ########################### */
        /*  RESULTADO DE LA SOLICITUD  */
        /* ########################### */

        // Codigo de respuesta
        int codigo;
        // Mensaje de respuesta
        string mensaje;
        // Estampa de tiempo del comienzo de la llamada
        string fechaInicioMinutaje;
        // Estampa de tiempo del final de la llamada
        string fechaFinMinutaje;
        // Contador de registros descargados
        int registrosDescargados;


        /* ############################# */
        /*  INFORMACION DE LA SOLICITUD  */
        /* ############################# */

        // Tipo de curva
        string tipoCurva;
        // Modo de lectura
        string modoLectura;
        // Fecha de inicio de la solicitud
        string fechaInicio;
        // Fecha de final de la solicitud
        string fechaFin;
        // Nombre del fichero CSV
        string nombreFicheroCSV;
        // Solicitud a la que hace referencia la solicitud de relleno
        int referencia;
        // Total de reintentos que debe hacer para considerar el hueco real
        int reintentos;
        // Vector de tuplas donde se almacenaran todas las fechas de las curvas descargadas
        vector<tuple<string, string>> vFechaCurvasDescargadas;
        // Vector de tuplas donde se almacenaran todas las fechas de los cierres descargados
        vector<tuple<string, string, int, int>> vFechaCierresDescargados;
        // Vector que nos dira cual sera la siguiente fecha de la curva a descargar
        vector<string> vSiguienteCurva;

    public:

        // Constructor por defecto
        Solicitud();

        // Metodo que añadira una fecha al vector de tuplas de fechas de curvas descargadas
        void addFechaCurva(string, string);

        // Metodo que añadira una fecha al vector de tuplas de fechas de cierres descargados
        void addFechaCierre(string, string, int, int);

        // Metodo que comprobara si existe una fecha en el vector de tuplas de fechas de curvas descargadas
        bool existeFechaCurva(string, string);

        // Metodo que comprobara si existe una fecha en el vector de tuplas de fechas de cierres descargados
        bool existeFechaCierre(string, string, int, int);

        // Metodo que devolvera a la solicitud a su estado por defecto
        void setDefault();

        // Metodo que aumentara en uno los registros descargados
        void aumentaRegistrosDescargados();

        void limpiaVectorFechaCurvas();

        // SETTERS
        void setCodigo(int);
        void setMensaje(string);
        void setFechaInicioMinutaje(string);
        void setFechaFinMinutaje(string);
        void setRegistrosDescargados(int);
        void setTipoCurva(string);
        void setModoLectura(string);
        void setFechaInicio(string);
        void setFechaFin(string);
        void setNombreFicheroCSV(string);
        void setReferencia(int);
        void setFechaSiguienteCurva(string, string);
        void setFechasCurvasDescargadas(vector<tuple<string, string>>);
        void setFechasCierresDescargados(vector<tuple<string, string, int, int>>);

        // GETTERS
        int getCodigo();
        string getMensaje();
        string getFechaInicioMinutaje();
        string getFechaFinMinutaje();
        int getRegistrosDescargados();
        string getTipoCurva();
        string getModoLectura();
        string getFechaInicio();
        string getFechaFin();
        string getNombreFicheroCSV();
        int getReferencia();
        vector<string> getUltimaCurvaDescargada();
        vector<string> getFechaSiguienteCurva();
        vector<tuple<string, string>> getFechasCurvasDescargadas();
        vector<tuple<string, string, int, int>> getFechasCierresDescargados();

};

#endif
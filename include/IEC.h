#ifndef _iec_h_
#define _iec_h_

#include "Archivo.h"
#include "Fecha.h"
#include "Comunicacion.h"
#include "Configuracion.h"
#include "Fija.h"
#include "Variable.h"
#include "Solicitud.h"

#include <vector>

using namespace std;

class IEC
{
    private:

        Comunicacion comunicacion;
        Fija fija;
        Variable variable;
        int reintento;
        Solicitud solicitud;

        string obtenerError(int);

    public:

        // Constructor por defecto
        IEC();

        // Metodo que analizara la trama devuelta por el contador y si tiene informacion, la extraera
        int analizar(int, vector<unsigned char>, vector<unsigned char>);

        // Metodo que realizara la descarga de informacion del contador
        void descarga();

        // Metodo que leera la configuracion del equipo
        void configuracion();

        // Metodo que reiniciara el modem
        void reinicio();

        // Metodo que descargara los instantaneos
        void instantaneos();

        // Metodo que comprobara el estado del suministro, ademas de validar los parametros
        void comprobar();

        // Metodo que devolvera la fecha completa del contador
        void hora();

};

#endif
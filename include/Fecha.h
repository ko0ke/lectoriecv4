#ifndef _fecha_h_
#define _fecha_h_

#include <string>
#include <vector>
#include <ctime>
#include <chrono>
#include <sstream>
#include <iomanip>
#include <bitset>
#include <iostream>

using namespace std;

class Fecha
{
    private:

        

        // Metodo que convertira un entero en dia
        static int intToDia(int);

        // Metodo que convertira un entero en hora con su bandera
        static vector<int> intToHora(int);

        static bool flagTM1;

        static bool flagTM2;

    public:

        // Constructor por defecto
        Fecha();

        // Metodo que convertira un string de fecha en hexadecimal
        static vector<unsigned char> fechaStrToFechaHex(string);

        // Metodo que convertira una fecha en hexadecimal en string
        static vector<string> fechaHexToFechaStr(vector<unsigned char>);

        // Metodo que nos dira la fecha de la siguiente curva a descargar
        static string siguienteFecha(vector<unsigned char>, string);

        // Metodo que nos dira la fecha de la siguiente curva a descargar
        static string siguienteFecha(string, string);

        // Metodo que nos dira cual debe de ser la fecha de la primera curva a descargar
        static string primerRegistro(string);

        // Metodo que nos dira si la fecha es valida
        static bool validaFecha(string);

        // Metodo que nos dira si la fecha incial es <= que la fecha final
        static int comparaFechas(string, string);

        // Metodo que convertira un string a entero
        static int stringToInt(string);

        // Metodo para convertir un entero a hexadecimal
        static unsigned char intToHex(int);

        // Metodo para convertir un hexadecimal a entero
        static int hexToInt(int);

        // Metodo que nos devolvera la fecha actual
        static string fechaActual();

        // Metodo que nos devolvera la estampa de tiempo de la fecha actual
        static string estampaFechaActual();

};

#endif
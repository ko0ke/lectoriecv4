#ifndef _fija_h_
#define _fija_h_

#include "Archivo.h"
#include "Configuracion.h"

using namespace std;

class Fija
{
    private:

        // CAMPOS DE CONTROL DEL MAESTRO
        static const string SOLICITUD_ESTADO_ENLACE;
        static const string REPOSICION_ENLACE_REMOTO;
        static const string SOLICITUD_DATOS;

        // Metodo para convertir un numero binario en string a hexadecimal
        int strBinToHex(string);

        // Metodo para convertir un entero a hexadecimal
        unsigned char intToHex(int);

        // Calculo del checksum de la trama
        unsigned char getChecksum(vector<unsigned char>);

    public:

        // FLAG DE TRAMA FIJA
        static const unsigned char FLAG_TRAMA_FIJA;

        // FLAG DE FIN DE TRAMA
        static const unsigned char FLAG_FIN;

        // TAMANO DE LA TRAMA FIJA
        static const int TAMANO_TRAMA_FIJA;

        // Bit de acuse de recibo
        static bitset<1> FRAME_COUNT_BIT;

        // Constructor por defecto
        Fija();

        // Metodos que montaran las tramas
        vector<unsigned char> solicitudEstadoEnlace();
        vector<unsigned char> reposicionEnlaceRemoto();
        vector<unsigned char> solicitudDatos();

};

#endif
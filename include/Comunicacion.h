#ifndef _comunicacion_h_
#define _comunicacion_h_

#include "Archivo.h"
#include "Configuracion.h"

#include <string>
#include <vector>

// Socket
#include <winsock2.h>

// Serial
#include <WinBase.h>

using namespace std;

class Comunicacion
{
    private:

        // Socket
        WSADATA wsaData;
        SOCKET fdSocket;
        struct sockaddr_in stSocket;

        // Serial
        DCB dcb;
        HANDLE hPuerto;

    public:

        // Constructor por defecto
        Comunicacion();

        // Metodo que abrira el Socket/Puerto Serie
        bool abrir();

        // Metodo que realizara la conexion entre los dos puntos
        bool conectar();

        // Metodo que realizara la llamada al modem en el caso de comunicacion GSM
        bool llamar();

        // Metodo que colgara la llamada con el modem en el caso de comunicacion GSM
        void colgar();

        // Metodo que enviara una trama al contador
        void enviar(vector<unsigned char>);

        // Metodo que recibira una trama del contador
        vector<unsigned char> recibir();

        // Metodo que enviara un comando AT al modem
        void enviar(string);

        // Metodo que recibira la respuesta a un comando AT
        bool recibir(string);

        // Metodo que cerrara el Socket/Puerto Serie
        void cerrar();

        // Metodo que modificara el timeout
        void setTimeout(int);

};

#endif
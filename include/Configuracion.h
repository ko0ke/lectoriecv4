#ifndef _configuracion_h_
#define _configuracion_h_

#include <iostream>

#include "Archivo.h"
#include "Solicitud.h"
#include "Fecha.h"

#include <string>
#include <vector>
#include <sstream>
#include <algorithm>

using namespace std;

class Configuracion
{
    private:

        Solicitud solicitud;

    public:

        // Constructor por defecto
        Configuracion();

        // Metodo que validara y recogera los parametros
        bool configurar(string);

        // Comunicacion GPRS
        static string ip;
        static int puerto;

        // Comunicacion GSM
        static string telefono;
        static string puertoCOM;

        // Parametros comunes
        static int dirEnlace;
        static int puntoMedida;
        static int clave;
        static string nombreFicheroLog;
        static int debug;

        // Operacion
        static string operacion;

        // Tipo de comunicacion que se va a realizar
        static string tipoComunicacion;

        // Objeto que almacenara una solicitud
        //Solicitud solicitud;

        // Vector que almacenara las solicitudes a realizar al contador
        static vector<Solicitud> vSolicitudes;

        string primerRegistro(string, string);
};

#endif
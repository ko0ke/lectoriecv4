# COMPILADOR
CC=g++
# OPCIONES
OPTIONS= -g -std=c++11
# DIRECTORIOS
LIBDIR=lib
INCLUDEDIR=include
BUILDDIR=build
BINDIR=bin
# OBJETOS
_OBJ=  Configuracion.o Solicitud.o Comunicacion.o IEC.o Fija.o Variable.o Archivo.o Fecha.o
OBJ = $(patsubst %,$(BUILDDIR)/%,$(_OBJ))

main:    main.cpp $(OBJ)
	$(CC) $(OPTIONS) -I$(INCLUDEDIR) main.cpp $(OBJ) -o $(BINDIR)/lector_iec_v4 -lws2_32

$(BUILDDIR)/%.o : $(LIBDIR)/%.cpp $(INCLUDEDIR)/%.h
	$(CC) $(OPTIONS) -Wall -w -c -I$(INCLUDEDIR) -o $@ $<
	
clean:
	@echo "del $(OBJ)"; del $(OBJ)
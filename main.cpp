#include "include/Configuracion.h"
#include "include/IEC.h"

// #include <iostream>

using namespace std;

int main(int argc, char *argv[]) {

    Configuracion configuracion;
    IEC iec;

    // Comprobamos que recibimos parametros
    if (argc > 1) {

        // Recogemos los valores pasados por parametro
        if (configuracion.configurar(argv[1]) == true) {

            //
            // Tipos de solicitud
            //
            // d -> descarga
            // s -> configuracion del contador
            // r -> reinicio del modem
            // i -> valores instantaneos
            // c -> comprobar contador
            // h -> fecha y hora del contador

            // Realizamos la operacion solicitada

            // Descarga
            if (Configuracion::operacion.compare("d") == 0) {
                
                iec.descarga();

            } 
            // Configuracion del contador
            else if (Configuracion::operacion.compare("s") == 0) {
                    
                iec.configuracion();

            } 
            // Reinicio del modem
            else if (Configuracion::operacion.compare("r") == 0) {

                iec.reinicio();

            } 
            // Valores instantaneos
            else if (Configuracion::operacion.compare("i") == 0) {
                
                iec.instantaneos();

            } 
            // Comprobar parametros del contador
            else if (Configuracion::operacion.compare("c") == 0) {
                 
                iec.comprobar();

            } 
            // Obtener hora del contador
            else if (Configuracion::operacion.compare("h") == 0) {
                  
                iec.hora();

            }
            
        }

    }
    
    return 0;
}